<?php
include "./sqlconfig.php";

$payment = $con->prepare("SELECT status, DATE(created_at) FROM `payment` WHERE username = ? ");
  $payment->bind_param('s', $_SESSION['name']);
  $payment->execute();
  $payment->store_result();
  $payment->bind_result($payment_status,$payment_date);
  $payment->fetch();
// echo $payment_status;
$_SESSION["payment_status"] = $payment_status;

  if($payment_status == "complete"){
    $_SESSION["payment_date"] = $payment_date;
    // echo "<script>window.location='./index.php'</script>";
  }

  else if($payment_status == "pending"){
    echo "<script>window.location='./payment.php'</script>";
  }

  else{
    echo "<script>window.location='./payment.php'</script>";
  }

?>