function student(){
    var studentDiv = document.getElementById('studentDiv');
    var regularDiv = document.getElementById('regularDiv');
    if ($('#student').is(':checked')) {
        studentDiv.style.visibility  = 'visible';
        regularDiv.style.visibility = 'hidden';
        console.log("Student registeration visible");
    }else{        
        studentDiv.style.visibility  = 'hidden';
        regularDiv.style.visibility = 'visible';
        console.log("Regular registeration visible");
    }
}

$("form#studentDiv").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
    console.log(formData);

    $.ajax({
        url: 'payment_process.php',
        type: 'POST',
        data: formData,
        success: function (data) {
            console.log(data);
            var options = {
                        "key": "rzp_live_USL9rr9xVNsUvJ", 
                        "amount": 700,
                        "currency": "USD",
                        "name": "Agriforetell",
                        "description": "1 year access to Agriforetell Snipe",
                        "image": "https://www.agriforetell.com/assets/img/aflogo2.png",
                        
                        "handler": function (response){
                        console.log(response);                            
                           jQuery.ajax({
                               type:'post',
                               url:'payment_process.php',
                               data:"payment_id="+response.razorpay_payment_id,
                               success:function(result){
                               alert('payment successful');
                                   window.location.href="index.php";
                               },
                               error:function(){
                                        window.location.href="loggedout.php";
                                    }
                           });
                           console.log(response);
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();

        },
        cache: false,
        contentType: false,
        processData: false
    });
});


$("form#regularDiv").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
    console.log(formData);

    $.ajax({
        url: 'payment_process2.php',
        type: 'POST',
        data: formData,
        success: function (data) {
            console.log(data);
            var options = {
                        "key": "rzp_live_USL9rr9xVNsUvJ", 
                        "amount": 1000, 
                        "currency": "USD",
                        "name": "Agriforetell",
                        "description": "1 year access to Agriforetell Snipe",
                        "image": "https://www.agriforetell.com/assets/img/aflogo2.png",
                        "handler": function (response){                            
                           jQuery.ajax({
                               type:'post',
                               url:'payment_process2.php',
                               data:"payment_id="+response.razorpay_payment_id,
                               success:function(result){
                                   alert('payment successful');
                                   window.location.href="index.php";
                               },
                               error:function(){
                                        window.location.href="loggedout.php";
                                    }
                           });
                           console.log(response);
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();

        },
        cache: false,
        contentType: false,
        processData: false
    });
});
