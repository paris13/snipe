$(function () {
    $('[data-toggle="tooltip"]').tooltip();
  })
$('#exportAllData').click(function(){
    exportallfunction();
});
$('#exportFarm').click(function(){
    exportfunction();
});
    var insightImage;
    var insightType="NDVI";
    var legend =  L.control({position: 'topright'});
    var currentFarm;
    var linechart;
    var timelineData;
    var currentDate = document.createElement("option");
  var sortIndex=1;
  var farms;
  var selectedFarmDates= new Array();
  var grapherOn = false; // variable-- if false graphering is called else changer is called,  these functions help with bar chart
  var timelineOn = false; //variable-- if false drawchart is called else linechange is called, these  function alter timeline chart 
  var geometry, farmID, latestDate, userid, area, minDate, polygonCentroid, polygonCentroidLat, polygonCentroidLon, markers;
var userFarmName;
  document.getElementById("crop").addEventListener('onclick', uno("crop"));
  document.getElementById("editCrop").addEventListener('onclick', uno("editCrop"));
  readKey();
  //------------------------------------------------------------------------------------------------------
  function uno(id) { // for loading different types of crops 
    
    var xh = new XMLHttpRequest();
      xh.open("GET", "./cropCategory.php", true);
      xh.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              cropList = JSON.parse(this.responseText);
              var crop = document.getElementById(id);
              for (i = 0; i < cropList.length; i++) {
                  option = document.createElement("option");
                  option.value = cropList[i];
                  option.innerHTML = cropList[i];
                  crop.appendChild(option);
              }
          }
  
      }
      xh.send();
  
  }

  //------------------------------------------------------------------------------------------------------
  // fetches number of fields for different users
  // TOdo get crop also
  
  function loadFields() {
    
      // farm selection view
    //   document.getElementById('weatherdash').style.display = "none";
      $("#akashList").empty();
    //   $("#timelines").hide(); //--hide today
    //   $("#fieldpopulate").show();
    //   $("#fieldselected").hide();
      $("#dialog-confirm").hide();
      $("#dialog-confirm-image").hide();
      var x = new XMLHttpRequest();
      x.open("GET", "./fields.php", true);
      x.onreadystatechange = function() {
          if(this.readyState == 4 && this.status == 200) {
              var fields = JSON.parse(this.responseText);
              console.log(fields);
              if(fields.length){
                showAddedFields();
                $("#farmMenuRow").show();
                $("#noaddedmessage").hide();
                $("#addedFieldsNav").disabled = false;
                $("#addedFieldsNav").removeClass("is-disabled");
                
              }

              else{
                $("#addedFieldsNav").addClass("is-disabled");
                hideElements();
                $("#farmMenuRow").hide();
                $("#noaddedmessage").show();
                $("#addFieldEmpty").show();
                $("#newFieldNav").addClass("active");
                $("#addedFieldsNav").disabled = true;
              }

              farms=fields;
              loadCentroids(farms);
              displayFields(sortIndex);
        }   
      }
      x.send();
  }
  
  function displayFields(sorterIndex){
      $(".field").remove();
      sortIndex=sorterIndex;
      console.log((sorter[sorterIndex]));
      farms.sort(sorter[sorterIndex]);
      for (i = 0; i < farms.length; i++) {
        createFarms(farms[i]);
      }
      var firstField = $("#fieldpopulate").children(":first");
      farmID = firstField.attr("name");
      console.log(farmID);
      firstField.addClass("active");
      console.log(firstField.attr("name"));
      loadFirstFarmDetails(firstField.attr("name"),firstField.attr("id"));
      // for (i = 0; i < farms.length; i++) {
      //     btn = document.createElement("DIV");
      //     btn.setAttribute("class", "col-8 userfields");
      //     // btn.setAttribute("onclick","fetcher(this)");
      //     btn.setAttribute("name", farms[i].fieldid);
      //     btn.innerHTML = "Field name: " + farms[i].fieldid + "<br> Crop: " + farms[i].crop + "<br> Sowing Date: " + farms[i].sowingDate + "<br> Area: " + farms[i].area + " acres";
      //     field.appendChild(btn);
      // }
  }


  function createFarms(farm){
    field = document.getElementById("fieldpopulate");

    var cover=document.createElement("div");
    cover.setAttribute("class","field list-group-item list-group-item-action flex-column align-items-start");
    cover.setAttribute("name",farm.fieldid);
    cover.setAttribute("id",farm.farmName);

    var title=document.createElement("div");
    // title.setAttribute("class","card-header");

    var spanTitle=document.createElement("span");
    spanTitle.setAttribute("class","instruction");
    /*  location field definition taken out 
    if((farm.location).length){
        console.log(farm.location);
      var loc = farm.location.split(" ");
      if(loc.length > 1){
          loc = loc.slice(1,loc.length).join("");
      }
    }
    console.log(loc);
    */
    spanTitle.innerHTML=farm.farmName;
    var content=document.createElement("div");
    // content.setAttribute("class","card-body");

    p1=document.createElement("p");
    p1.setAttribute("class","fielddetails");
    p1.innerHTML=farm.crop + ", " + farm.area + " acres<br> Sowing date : " + farm.sowingDate;

    // p2=document.createElement("p");
    // p2.innerHTML="Sowing Date : " + farm.sowingDate;
    // p3=document.createElement("p");
    // p3.innerHTML="Area : " + farm.area + " acres";

    title.append(spanTitle);
    content.append(p1);
    cover.append(title,content);
    // cover.append(content);
    field.appendChild(cover);
    // uno.appendChild
  }
  
  loadFields();
  

  $(document).on('click','.field' ,function(){
    $('.field').removeClass("active");
    $(this).addClass('active');
    hideElements();
    stopEditing();
    try{
        map.removeLayer(markers);
    }
    catch{
        console.log("can't remove markers");
    };
    $("#map").css('height','80%');
    $("#boxTimeline").show();
    $("#fieldDetails").show();

      var xh = new XMLHttpRequest();
      xh.open("GET", "./geometry.php?fieldId=" + this.getAttribute('name'), true);
      xh.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              var res = JSON.parse(this.responseText);
              geometry = (res[0].geometry);
              mapping(geometry);
              if (geometry != undefined){
                prepareWeatherCall();
              }
          }
      }
      xh.send();
    //   $("#fieldpopulate").hide();  //-----------hide today
    //   $("#fieldselected").show();  //-----------hide today
     fetcher(this); 
     loadCentroids(farms);
     
  });

  function loadFirstFarmDetails(farmName,userFarmName){
    $("#fieldName").html(userFarmName);
    $("#map").css('height','80%');
    $("#boxTimeline").show();
    $("#fieldDetails").show();
      var xh = new XMLHttpRequest();
      xh.open("GET", "./geometry.php?fieldId=" + farmName, true);
      xh.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              var res = JSON.parse(this.responseText);
              geometry = (res[0].geometry);
              mapping(geometry);
              if (geometry != undefined){
                prepareWeatherCall();
              }
          }
      }
      xh.send();
    //   $("#fieldpopulate").hide();  //-----------hide today
    //   $("#fieldselected").show();  //-----------hide today
     fetchresults(farmName); 
  }
  
  $("#backbuttondiv").click(function() {  
    showAddedFields();
    goBackTofarms();
  });
  

  $('.showAddedFields').click(function(){
    showAddedFields();
    if(farms.length > 0){
    loadCentroids(farms);
    };
});

  function goBackTofarms() {
      loadFields();
  
     $("#line").empty(); 
    //   $("#graph").empty(); // hide today
      try {
          mainbtn.innerHTML = "";
          mainbtn.style.display = "none";
      } catch {
          console.log("mainbtn not yet clicked");
      }
      grapherOn = false;
      timelineOn = false;
      removeExistingImages();
      removeExistingPolygons();
  }
  
  //------------------------------------------------------------------------------------------------------
  
  function fetcher(ele) {
      //creates buttons for fields from data collected by satelite for chosen field
      // timeline data for certain fields of certain users 
      // ele is field attribute from which the request is sent
    //   document.getElementById("weatherdash").style.display="block"; //--hide today
      $("#calculatedData").show();
    //   $("#timelines").show(); //--hide today
      farmID = ele.getAttribute("name");
      userFarmName = ele.getAttribute("id");
      $("#fieldName").html(userFarmName);
      var x = new XMLHttpRequest();
      //FIXME Update satelitescans php
      x.open("GET", "./sateliteScans.php?fieldId=" + ele.getAttribute("name"), true);

      x.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              var fieldData = JSON.parse(this.responseText);
              console.log(fieldData);
              fieldDates = [];
              selectedFarmDates=[];
              for (i = 0; i < fieldData.length; i++) {
                  fieldDates[i] = fieldData[i].substring(10, 0);
              }
              selectedFarmDates=fieldDates;
              if (fieldData.length) {
                 /* $("#fielddates").html("");
                  d3.select("#fielddates").selectAll(".satbtn").remove();
                  d3.select("#fielddates").selectAll("#deletesatbtn").remove();
                  d3.select("#fielddates").selectAll(".satbtn").data(fieldDates)
                      .enter().append("button")
                      .attr("class", "satbtn btn ")
                      .attr("onclick", "figures(this)")
                      .attr("name", ele.getAttribute("name"))
                      .html(function(d) { return d; });
                        $("#graph").show();
                        $('#line').show();
  
                  d3.select("#fielddates").append("button").attr("id", "deletesatbtn")
                      .attr("class", "btn btn-danger").attr("onclick", "deletefieldcreate()")
                      .html('Delete Data'); */
  
                    timelineCaller(ele);
                    $("#dwonloadBtn").show();
              } else {
                  $("#fielddates").html("No satellite images found");
                  $("#graph").hide();
                  $('#line').hide();
                  $("#map").css('height','100%');
                  $("#boxTimeline").hide();
                  $("#ndviTitle").html(" No Data available for this farm");
                  $("#dwonloadBtn").hide();
                  // document.getElementById("dwonloadBtn").disabled = true;

              }
  
              getMaxDate(ele.getAttribute("name"));
              getMinDate(ele.getAttribute("name"));
  
              // function timeline caller is being called. 
              //It either calls function drawchart or calls function linechange depending upon state of lineOn  
              // d3.select("#timelinebutton").remove();
              // d3.select("#fielddates").append("button").attr("onclick","timelineCaller(this)")
              // .attr("id","timelineButton").html(ele.innerHTML);
          }
      }
      x.send();
     
       
  }
  
  
  function fetchresults(f) {
    //   alert("fetch results work");
      //creates buttons for fields from data collected by satelite for chosen field
      // timeline data for certain fields of certain users 
      // ele is field attribute from which the request is sent
      $("#calculatedData").show();
    //   $("#timelines").show(); //--hide today
      var x = new XMLHttpRequest();
      x.open("GET", "./sateliteScans.php?fieldId=" + f, true);
  
      x.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            try {
              var fieldData = JSON.parse(this.responseText);
              fieldDates = [];
              selectedFarmDates=[];
              for (i = 0; i < fieldData.length; i++) {
                  fieldDates[i] = fieldData[i].substring(10, 0);
              }
              selectedFarmDates=fieldDates;
              if (fieldData.length) {
                
                  /* $("#fielddates").html("");
                  d3.select("#fielddates").selectAll(".btn").remove();
                  d3.select("#fielddates").selectAll("#deletesatbtn").remove();
                  d3.select("#fielddates").selectAll(".btn").data(fieldDates)
                      .enter().append("button")
                      .attr("class", "satbtn")
                      .attr("onclick", "figures(this)")
                      .attr("name", f)
                      .html(function(d) { return d; });
                    $("#graph").show();
                    $('#line').show(); 
  
                    d3.select("#fielddates").append("button").attr("id", "deletesatbtn")
                      .attr("class", "btn btn-danger").attr("onclick", "deletefieldcreate()")
                      .html('Delete Data'); */
                      $("#map").css('height','80%');
                      $("#boxTimeline").show();
                      $("#line").show();
                      $("#ndviTitle").html("");
                  timelineCreator(f);
                  $("#dwonloadBtn").show();
  
              } else {
                  $("#fielddates").html("No satellite images found");
                  $("#graph").hide();
                  $('#line').hide();
                  $("#map").css('height','100%');
                  $("#boxTimeline").hide();
                  $("#ndviTitle").html(" No Data available for this Farm");
                  $("#dwonloadBtn").hide();
  
              }
              getMaxDate(f);
              getMinDate(f);
              // function timeline caller is being called. 
              //It either calls function drawchart or calls function linechange depending upon state of lineOn  
              // d3.select("#timelinebutton").remove();
              // d3.select("#fielddates").append("button").attr("onclick","timelineCaller(this)")
            }  // .attr("id","timelineButton").html(ele.innerHTML);
            catch (Error){
            
            }
          }
      }
      x.send();
  }
  
  //------------------------------------------------------------------------------------------------------
  
  
  function figures(fieldDateButtons) {
      /* fetches the data from backend for barchart and calls grapher or change
      depending upon the state of the grapheron variable*/
      $('.highlightedDate').removeClass("highlightedDate");
      $(this).addClass("highlightedDate");
      fieldDateButtons.classList.add("highlightedDate");
      var xml = new XMLHttpRequest();
      xml.open("GET", "./figure.php?fieldName=" + fieldDateButtons.name + "&fieldDate=" + fieldDateButtons.innerHTML, true);
      xml.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
              var figureData = JSON.parse(this.responseText);
              
              
              $("#graph").show();
              if (figureData.myUrl) {
                insightImage=figureData.myUrl;
                insightType=checkRadios(insightImage,insightType); 
                drawImageonMap(insightImage[insightType], geometry);
              }
          }
      }
  
      xml.send();
  
  }
  
  //------------------------------------------------------------------------------------------------------
  
  function changer(myData) {
      /* it changes the bar chart appearance from the given data  */
      d3.selectAll(".bars").data(myData).attr('transform', function(d, i) {
          return "translate(" + i * 80 + "," + (300 - d * 3) + ")";
      })
      d3.selectAll(".barcolumn").data(myData).attr('height', function(d, i) { return d * 3; });
  
  }
  
  //------------------------------------------------------------------------------------------------------
  
  function graphering(myData) {
      /*creates a new bar chart from the data given
      called only when grapherOn variable is set false*/
      var margin = { top: 20, right: 30, bottom: 30, left: 40 },
          width = myData.length * 80;
      height = 400 - margin.top - margin.bottom;
      var sv = d3.select("#graph").append('svg')
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
      // sv = d3.select('#d3uno').append('svg').attr('height',500).attr('width',data.length*40+40);
  
      bar = sv.selectAll("g")
          .data(myData)
          .enter()
          .append("g").classed('bars', true)
          .attr("transform", function(d, i) {
              return "translate(" + i * 80 + "," + (300 - d * 3) + ")";
          });
  
      var rect = bar.append('rect').classed('barcolumn', true).attr('width', 50).attr('height', function(d) {
          return 3 * d;
      }).attr("fill", "#ff72ad");
  
  
      var scale = d3.scaleLinear()
          .domain([0, 100])
          .range([0, 300]);
  
      // Add scales to axis
      var y_axis = d3.axisBottom()
          .scale(scale);
  
      //Append group and insert axis
      sv.append("g")
          .call(y_axis).attr('transform', ' translate(-10,300) rotate(270)');
  
      var x = d3.scaleBand()
          .domain(["Good", "Normal", "Bad"])
          .rangeRound([0, width]);
  
      var x_axis = d3.axisBottom().scale(x);
      var classer = sv.append("g");
      classer.classed('newclass', true);
      classer.call(x_axis).attr('transform', ' translate(-10,300) ');
  
  }
  
  //------------------------------------------------------------------------------------------------------
  
  function timelineCaller(ele) { //gathers timeline data from timeline.php and calls the drawChart
      //    alert('timelinecaller called');
      listCreator(ele.getAttribute("name"));
      $(".satbtn").hide();
      var req = new XMLHttpRequest();
      req.open("GET", "./timeline.php?fieldid=" + ele.getAttribute("name"), true);
      req.onreadystatechange = function() {
          if (this.readyState == 4 & this.status == 200) {
              var timelineData={"NDVI":new Array(),"NDWI":new Array};
              var timeline = JSON.parse(this.responseText);
              console.log(timeline);
              for (i = 0; i < timeline.myData.length; i++) {
                  var obj = new Object();
                  obj.x = new Date(timeline.myDate[i]);
                  obj.y = timeline.myData[i];
                  var ndwi = new Object();
                  ndwi.x = new Date(timeline.myDate[i]);
                  ndwi.y = timeline.myNdwi[i];
                  timelineData['NDVI'][i] = obj;
                  timelineData['NDWI'][i]=ndwi;
                }
              makeTimelineGlobal(timelineData);
              chartCreator();
            }
      }
  
      req.send();
  
  }
  
  function getMaxDate(f) {
      var xh = new XMLHttpRequest();
      xh.open("GET", "./max_timefetcher1.php?field_id=" + f, true);
      xh.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              try {
                  maxDateList = JSON.parse(this.responseText);
                  latestDate = addDays(maxDateList[1], 1).substring(10, 0).replace(/-/g, "");
                  // latestDate =(parseInt(abc)+1).toString();
                  // currentImage = maxDateList[2];
                  // if(currentImage){
                  // drawImageonMap(currentImage,geometry);
                  // }
              } catch {
                  console.log("error");
              }
          }
      };
  
      xh.send();
  };
  
  function getMinDate(f) {
      var xh = new XMLHttpRequest();
      xh.open("GET", "./min_timefetcher.php?field_id=" + f, true);
      xh.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              try {
                  minDateList = JSON.parse(this.responseText);
                  console.log(minDateList);
                  currentDate.innerHTML = minDateList[1].substring(10, 0);
                  minDate = minDateList[1].substring(10, 0).replace(/-/g, "");
                  // latestDate =(parseInt(abc)+1).toString();
                  insightImage = minDateList[2];
                  checkRadios(insightImage);
                  if (insightImage) {
                      drawImageonMap(insightImage[insightType], geometry);
                  }
              } catch {
                  console.log("error");
              }
          }
      };
  
      xh.send();
  };
  
  
  function timelineCreator(fieldName) { //gathers timeline data from timeline.php and calls the drawChart
      // timelineOn=false;
      listCreator(fieldName);
      $(".satbtn").hide();
      var req = new XMLHttpRequest();
      req.open("GET", "./timeline.php?fieldid=" + fieldName, true);
      req.onreadystatechange = function() {
          if (this.readyState == 4 & this.status == 200) {
              var timeline = JSON.parse(this.responseText);
              var timelineData={"NDVI":new Array(),"NDWI":new Array};
              for (i = 0; i < timeline.myData.length; i++) {
                  var obj = new Object();
                  obj.x = new Date(timeline.myDate[i]);
                  obj.y = timeline.myData[i];
                  var ndwi = new Object();
                  ndwi.x = new Date(timeline.myDate[i]);
                  ndwi.y = timeline.myNdwi[i];
                  timelineData['NDVI'][i] = obj;
                  timelineData['NDWI'][i]=ndwi;
                }
                console.log(timelineData);
              makeTimelineGlobal(timelineData);
              chartCreator();
            }
        }
  
      req.send();
  
  }
  
  //------------------------------------------------------------------------------------------------------
  
  function drawChart(timelineData) {
      /* creates a new linechart accoriding to the timelinedata provided as parameter provided to this function
       when called upon. Called only when the linOn variable is set to false */
      $("#line").remove("svg");
      var svgWidth = 400,
          svgHeight = 800;
      var margin = { top: 20, right: 80, bottom: 30, left: 30 };
      var width = svgWidth - margin.left - margin.right;
      var height = svgHeight - margin.top - margin.bottom;
  
      var svg = d3.select('#line').append("svg")
          .attr("width", svgWidth)
          .attr("height", svgHeight+15);
  
      g = svg.append("g")
          .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");
  
      x1 = d3.scaleTime().rangeRound([0, width]);
      y1 = d3.scaleLinear().rangeRound([height, 0]);
  
      line = d3.line()
          .x(function(d) { return x1(d.date); })
          .y(function(d) { return y1(d.value); });
  
      x1.domain(d3.extent(timelineData, function(d) { return d.date }));
      y1.domain([0, 1]);
      // y1.domain(d3.extent(timelineData, function(d) { return d.value }));
      x1.nice();
      y1.nice();
      xCall=g.append("g").classed("xaxis",true)
          .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x1).tickFormat(d3.timeFormat("%b %d")));

        xCall.selectAll("text")
          .attr("y", 0)
          .attr("x", 9)
          .attr("dy", ".35em")
          .attr("transform", "rotate(90)")
          .style("text-anchor", "start");
          
    // xCall.select('.domain').remove();

  
      g.append("g").classed("yaxis", true)
          .call(d3.axisLeft(y1))
          .append("text")
          .attr("fill", "#ddd")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "0.71em")
          .attr("text-anchor", "end")
          .text("NDVI (avg)");
  
      g.append("path").classed("pather", true)
          .datum(timelineData)
          .attr("fill", "none")
          .attr("stroke", "#e62027")
          .attr("stroke-linejoin", "round")
          .attr("stroke-linecap", "round")
          .attr("stroke-width", 2)
          .attr("d", line);

         hover =g.append('g').classed("hovertip",true);
  
      g.selectAll("data-circle")
          .data(timelineData)
          .enter().append("circle")
          .attr("class", "data-circle").on("mouseover",mouser).on("mouseout",mouse)
          .attr("r", 3)
          .attr("cx", function(d) { return x1(d.date); })
          .attr("cy", function(d) { return y1(d.value); })
          .attr("stroke-width", "2px").attr("fill" ,"#fff").attr("stroke","#e62027");


  
  }
  //------------------------------------------------------------------------------------------------------
  
  function linechange(timelineData) {
    //    alert("linechange is called");
      /*changes the line chart according to the given data when called upon.
          called only when lineOn varible is set to true */
      line = d3.line()
          .x(function(d) { return x1(d.date); })
          .y(function(d) { return y1(d.value); });
  
      g.selectAll(".data-circle").remove();
      //x1.domain(d3.extent(timelinetimelineData, function(d) { return d.date }));
      // y1.domain([0,d3.max(timelineData, function(d) { return d.value })]);
      y1.domain([0, 1]);
      x1.domain(d3.extent(timelineData, function(d) { return d.date }));
      y1.nice();
      x1.nice();

      d3.select(".xaxis").call(d3.axisBottom(x1).tickFormat(d3.timeFormat("%b %d")));
      xCall.selectAll("text")
          .attr("y", 0)
          .attr("x", 9)
          .attr("dy", ".35em")
          .attr("transform", "rotate(90)")
          .style("text-anchor", "start");

      d3.select(".yaxis").call(d3.axisLeft(y1));
      d3.select(".pather").datum(timelineData)
          .attr("fill", "none")
          .attr("stroke", "#e62027")
          .attr("stroke-linejoin", "round")
          .attr("stroke-linecap", "round")
          .attr("stroke-width", 2)
          .attr("d", line);
  
          g.selectAll("data-circle")
          .data(timelineData)
          .enter().append("circle")
          .attr("class", "data-circle").on("mouseover",mouser).on("mouseout",mouse)
          .attr("r", 3)
          .attr("cx", function(d) { return x1(d.date); })
          .attr("cy", function(d) { return y1(d.value); })
          .attr("stroke-width", "2px").attr("fill" ,"#fff").attr("stroke","#e62027");
  }
  
  //------------------------------------------------------------------------------------------------------

  var map = L.map('map', {
    center: [0, 0],
    zoom: 2,
    zoomControl: true
});
    map.setMaxZoom(21);
    var hybrid = L.gridLayer.googleMutant({ type: 'hybrid' }).addTo(map);
  
    // var geocoder = new google.maps.Geocoder();

    // function googleGeocoding(text, callResponse)
	// {
	// 	geocoder.geocode({address: text}, callResponse);
    // }
    
    // function formatJSON(rawjson)
	// {
	// 	var json = {},
	// 		key, loc, disp = [];

	// 	for(var i in rawjson)
	// 	{
	// 		key = rawjson[i].formatted_address;
			
	// 		loc = L.latLng( rawjson[i].geometry.location.lat(), rawjson[i].geometry.location.lng() );
			
	// 		json[ key ]= loc;	//key,value format
	// 	}

	// 	return json;
	// }

    // map.addControl( new L.Control.Search({
    //     sourceData: googleGeocoding,
    //     formatData: formatJSON,
    //     markerLocation: true,
    //     autoType: false,
    //     autoCollapse: true,
    //     minLength: 2
    // }) );

  var searchControl = new L.Control.Search({
      url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
      jsonpParam: 'json_callback',
      propertyName: 'display_name',
      propertyLoc: ['lat', 'lon'],
      // marker: L.circleMarker([0,0],{radius:3}),
      markerLocation: true,
      autoCollapse: true,
      autoType: false,
      minLength: 2,
      zoom: 13,
      text: 'Navi Mumbai...',
      textCancel: 'Cancel Search',
      textErr: 'Error'
  });

  
  map.addControl(searchControl);

  function addLegend(insightType){
    try{
        console.log(legend);
        map.removeControl(legend);
    }
    catch{
        console.log("Cannot remove legend");
    }
   

    legend.onAdd = function (map){

    if(insightType=="NDVI"){
    var div = L.DomUtil.create('div', 'info legend'),
        grades = ["Healthy growth", "Moderate growth", "No growth"],
        colors = ['#006600', '#f7f600', '#995200'],
        labels = [];

    }

    else if(insightType=="NDWI"){
        var div = L.DomUtil.create('div', 'info legend'),
            grades = ["High moisture", "Moderate moisture", "Low moisture"],
            colors = ['#0000FF', '#00FFFF', '#FF0000'],
            labels = [];
    
        }

        else if(insightType=="TRUE"){
            var div = L.DomUtil.create('div', 'info legend'),
                grades = ["True color image"],
                colors = ['#FFFFFF'],
                labels = [];
        
            }

    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + colors[i] + '"></i>' + grades[i] + '<br>';
    }

    return div;
}

legend.addTo(map);
}



  function mapping(polyJSON) {
      removeExistingPolygons();
      addLegend(insightType);
      polyJSON = JSON.parse(polyJSON)
      var latlngs = polyJSON.features[0].geometry.coordinates;
      var coords = latlngs[0].slice(0, latlngs[0].length - 1);
      var coordinates = [];
      for (i = 0; i < coords.length; i++) {
          lon = coords[i][0];
          lat = coords[i][1];
          coordinates.push([lat, lon]);
      };
      polygon = L.polygon(coordinates, { color: '#06d633', fillOpacity: '0' }).addTo(map);
      map.fitBounds(polygon.getBounds(),6);
  }
  
  function removeExistingPolygons() {
      map.eachLayer(function(layer) {
          if (layer instanceof L.Polygon || layer instanceof L.CircleMarker) {
              console.log(layer);
              map.removeLayer(layer);
          }
          setPolygonDrawn(false);
      });
      try{
      map.removeControl(legend);
    //   map.removeLayer(markers);
      }
      catch {

      }
      map.setView([0,0], 2);
      removeExistingImages();
  };
  
  function drawImageonMap(myUrl, polygon) {
  
      geoPoly = L.geoJson(JSON.parse(polygon));
      bounds = geoPoly.getBounds();
      var imagelayer = L.imageOverlay(myUrl, bounds);
      console.log(typeof(myUrl));
      $.get(myUrl, function(data, status){
        removeExistingImages();
          var svgelement = data.documentElement;
        console.log(svgelement);
            try {
                if(insightType!="TRUE"){
                    var svglayer = L.svgOverlay(svgelement, bounds).addTo(map);
                console.log(svglayer);
                    }
                else if(insightType=="TRUE"){
                  console.log(insightType);
                imagelayer.addTo(map);
                console.log(imagelayer);
                }
            } catch {
                console.log("Cannot add image on map");
            }
        
      });

    //   removeExistingImages();
      
  }
  
  function removeExistingImages() {
      map.eachLayer(function(layer) {
          if (layer instanceof L.ImageOverlay) {
              try{map.removeLayer(layer);}
              catch(error){
                  console.log(error);
                }
          }
      });
  };
  
  $("#addedFieldsNav").click(function(){
      removeExistingPolygons();
      stopEditing();
  });
  
  /* ----------- Creating fields ------------- */
  
  var newField = document.getElementById("newFieldRegistration");
  var fields = document.getElementById("fieldsContainer");
  var drawnItems, drawControlFull, drawControlEditOnly, drawnPolygon;
  var polygonCreated = false;
  
  $("#saveField").click(function() {
      fieldCreate();
      map.setMaxZoom(17);
  });
  
  $("#cancelSaveField").click(function() {
      $("#submitstatus").hide();
    removeExistingPolygons();
    stopEditing();
    map.setMaxZoom(17);
  });
  
  function fieldCreate() {
      /* function for creating new field in the database */
      var farmName = document.getElementById("farmName");
      var location = document.getElementById("location");
      var crop = document.getElementById("cropInput");
      var sowingDate = document.getElementById("sowingdate");
        
      if (farmName.value == '' ||crop.value == '' || sowingDate.value == '' || !(polygonCreated)) {
          $('#submitstatus').html("Please enter all details and then save");
      } else {
        sowingDate.value=checkSD(sowingDate.value);
          data = "farmName="+ farmName.value+"& location=" + location.value + "& crop=" + crop.value + "& sowingdate=" + sowingDate.value + "& geometry=" + JSON.stringify(drawnPolygon) + "& area=" + area + "& latitude=" + polygonCentroidLat + "& longitude=" + polygonCentroidLon;
          $('#submitstatus').html("");
          $.post('./FieldCreate.php', data, function(result) {
              alert(result);
              showAddedFields();
              loadCentroids(farms);
          });
  
          setTimeout(displaySwitchBack, 1000);
      }
  }
  
  function Switch() {
      /* for switching from fields to create field*/
      removeExistingPolygons();
      startEditing();
      fields.style.display = "none";
      newField.style.display = "block";
  }
  
  function displaySwitchBack() {
      /* opposite of switch function-> switches from create field to existing field window*/
    //   $(this).addClass('active');
    
      stopEditing();
      try{
        map.removeLayer(markers);
    }
    catch{
        console.log("can't remove markers");
    };
      removeExistingPolygons();
      loadFields();
      try{
        hideElements();
    //   newField.style.display = "none";
    //   fields.style.display = "block";
      }
      catch {

      };
  }
  
  // Start editing function
  
  // FeatureGroup is to store editable layers
  
  function startEditing() {
      drawnItems = new L.FeatureGroup();
      map.addLayer(drawnItems);
  
      drawControlFull = new L.Control.Draw({
          edit: {
              featureGroup: drawnItems
          },
          draw: {
              polyline: false,
              marker: false,
              circlemarker: false,
              circle: false
          }
      });
  
  
      drawControlEditOnly = new L.Control.Draw({
          edit: {
              featureGroup: drawnItems
          },
          draw: false
      });
  
      map.addControl(drawControlFull);
      map.on("draw:created", function(e) {
          var layer = e.layer;
          layer.addTo(drawnItems);
          drawControlFull.remove(map);
          drawControlEditOnly.addTo(map);
          drawnPolygon = drawnItems.toGeoJSON();
          setPolygonDrawn(true);
          area = (L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]) * 0.000247105).toFixed(2);
          polygonCentroid = layer.getBounds().getCenter();
          polygonCentroidLat = polygonCentroid.lat;
          polygonCentroidLon = polygonCentroid.lng;

      });
  
      map.on("draw:deleted", function(e) {
          check = Object.keys(drawnItems._layers).length;
          if (check === 0) {
              drawControlEditOnly.remove(map);
              drawControlFull.addTo(map);
              setPolygonDrawn(false);
          };
      });
  };
  
  // Stop editing
  
  function stopEditing() {
      try{
        drawControlEditOnly.remove(map);
        drawControlFull.remove(map);
      }
      catch {
          
      }
     
  };
  
  function setPolygonDrawn(isDrawn) {
      if (isDrawn) {
          polygonCreated = true;
          $('#polyspan').css("color", "green");
          $("#polyspan").html(" Farm drawn on the map ");
          $('#polygonradio').prop("checked", true);
      } else {
          polygonCreated = false;
          $('#polyspan').css("color", "red");
          $("#polyspan").html(" Draw your farm on the map ");
          $('#polygonradio').prop("checked", false);
      }
  }
  
  
  /* --------- Retrieving results ----------- */
  
  var fetchButton = document.getElementById("fetch");
  fetchButton.addEventListener("click", function() {
      getResults(geometry);
  });
  
  function getResults(polygon) {
      var apiJSON, ingestionError;
      $("#spinner").show();
    
        
      $("#modal1").html("Analyzing your farm health ..");
      $("#modal2").html("Please do not refresh the page or go back while processing");
      $('#processLoader').dialog({
          escapeClose: false,
          clickClose: false,
          showClose: false,
          modal: true,
          buttons: {
            "Cancel Processing": function() {
              $( this ).dialog( "close" );
            },
            Ok: function() {
              $( this ).dialog( "close" );
            }
          }
      });
      $("#processLoader").parent().find(".ui-dialog-titlebar-close").hide();
      fetchButton.disabled = true;
      $(":button:contains('Ok')").prop("disabled", true).addClass("ui-state-disabled");
      apiJSON = JSON.parse(polygon);
      apiJSON['features'][0]['properties']['farmID'] = farmID;
      apiJSON['features'][0]['properties']['latestDate'] = latestDate;
      var apiUrl='./try.php';
      try {
            // console.log(apiJSON);
           let xh =new XMLHttpRequest();
           xh.open("POST",apiUrl,true);
           xh.setRequestHeader('Content-Type', 'application/json');
           xh.send(JSON.stringify(apiJSON));
           xh.onprogress = function(event){
                // fetchresults(farmID);
                console.log(event);
            }
           xh.onreadystatechange=function(){ 
               if(this.readyState==4 && this.status==200){
                //    console.log(this.responseText);
                var one  = this.responseText.includes("Your connection is not active now please recharge your account") ? "Your connection is not active now please recharge your account": "Processing Complete"
                    fetchButton.disabled=false;
                    $("#spinner").hide();
                    $(":button:contains('Cancel Processing')").prop("disabled", true).addClass("ui-state-disabled");
                    $(":button:contains('Ok')").prop("disabled", false).removeClass("ui-state-disabled");
                    $("#modal1").html(one);
                    $("#modal2").html("");
                    $("#akashList").empty();
                    fetchresults(farmID);
                }
            }
            
        //   fetch(apiUrl, {
        //           method: 'POST', // or 'PUT'
        //           body: JSON.stringify(apiJSON), // data can be `string` or {object}!
        //           headers: {
        //               'Content-Type': 'application/json'
        //           }
        //       })
        //       .then((resp) => resp.json())
        //       .then(function(data) {
        //           if(data['status'] == "Success"){
        //           saveResultToDB(data);
        //           $("#spinner").hide();
        //         //  $("#cp").html("OK");
                // $(":button:contains('Cancel Processing')").prop("disabled", true).addClass("ui-state-disabled");
                // $(":button:contains('Ok')").prop("disabled", false).removeClass("ui-state-disabled");
                  
                // $("#modal1").html("Processing complete");
                //   $("#modal2").html("");
                //   fetchButton.disabled = false;
                //   }
        //           else if (data['status'] == "Error"){
        //           $("#spinner").hide();
        //         //   $("#cp").html("OK");
        //         $(":button:contains('Cancel Processing')").prop("disabled", true).addClass("ui-state-disabled");
        //         $(":button:contains('Ok')").prop("disabled", false).removeClass("ui-state-disabled");
        //           for(i=0;i<data['messages'].length; i++){
        //               if(data['messages'][i] == "'ingestiondate'"){
        //                   ingestionError = true;
        //               }
        //           }
        //           if(ingestionError){
        //           $("#modal1").html("Latest non-cloudy imagery is unavailable. Kindly try after a few days.");
        //           }
        //           else {
        //               console.log(data);
        //           $("#modal1").html("An unexpected error occured. Kindly try again after some time");
        //           }
        //           $("#modal2").html("");
        //           fetchButton.disabled = false;
        //           }
        //       })
        //       .catch(error => console.log('Error:', error));
      } catch (err) {
          $("#spinner").hide();
          $("#cp").html("OK");
          $("#modal1").html("An error occured");
          $("#modal2").html("");
          fetchButton.disabled = false;
      }
  };
  
  
  function saveResultToDB(JSONdata) {
      data = "jsondata=" + JSON.stringify(JSONdata) + "&farmID=" + farmID;
      $.post('./save.php', data, function(result) {
          console.log(result);
  
      });
  };
  
  /*----------- Progress loader ----------- */
  
  $("#cp").click(function() {
      fetchButton.disabled = false;
      $("#akashList").empty();
    //   $("#timelines").hide(); //--hide today
      fetchresults(farmID);
      // timelineCreator(farmID);
  });
  
  
  $("#deleteField").click(function() {
      $("#dialog-confirm").dialog({
          resizable: false,
          height: "auto",
          width: 400,
          modal: true,
          buttons: [{
                  text: 'Yes, delete field',
                  class: 'btn-danger',
                  click: function() {
                      var x = new XMLHttpRequest();
                      x.open("GET", "./deletefarm.php?fieldid=" + farmID, true);
                      x.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var result = this.responseText;
                              if (result.includes("success")) {
                                  $("#dialog-confirm").dialog("close");
                                  alert("Farm " + farmID + " deleted");
                                  goBackTofarms();
                              } else {
                                  $("#dialog-confirm").dialog("close");
                                  alert("Unable to delete farm " + farmID);
                              }
                          }
                      }
                      x.send();
                      ``
                  }
              },
              {
                  text: 'No, cancel',
                  class: 'btn-default',
                  click: function() {
                      $(this).dialog("close");
                  }
              }
          ]
      });
  });
  
  function exportfunction() {
  
      var link= "./excelexport.php?fieldid=" + farmID;
      var anchor = document.createElement("a");
      anchor.setAttribute("href",link);
      anchor.click();
      anchor.style.display = 'none';
      document.body.appendChild(anchor);
      anchor.click();
      document.body.removeChild(anchor);

  }
  
  function exportallfunction() {
  
      window.location = "./excelexportall.php";
  
  }
  
  function deletefieldcreate() {
      var deleteData = document.getElementById('deletesatbtn');
      deleteData.disabled = true;
      mainbtn = document.getElementById('whateverdiv');
      mainbtn.innerHTML = "";
      mainbtn.style.display = "block";
      var buttonarray = selectedFarmDates; //document.getElementsByClassName("satbtn");
      // buttonarray=['one', 'two', 'three','four'];
      for (i = 0; i < buttonarray.length; i++) {
            li=document.createElement("li");
          checkbtn = document.createElement("input");
          checkbtn.setAttribute('class', 'satbtndelete');
          checkbtn.type = "checkbox";
          checkbtn.name = "satbtnchecks";
          checkbtn.value = buttonarray[i];
          checkbtn.id = buttonarray[i]; 
          var label = document.createElement('label');
          label.htmlFor = buttonarray[i];
          label.appendChild(document.createTextNode(buttonarray[i]));
  
          li.appendChild(checkbtn);
          li.appendChild(label);
          mainbtn.append(li);
  
  
      }
  
      var deletebutton = document.createElement("button");
      deletebutton.setAttribute("class", "satbtnkill btn-danger btn");
      deletebutton.setAttribute("onclick", "imageFieldDelete()");
      deletebutton.innerHTML = "Delete Dates";
      mainbtn.appendChild(deletebutton);
  
      var cancelDeleteButton = document.createElement("button");
      cancelDeleteButton.setAttribute("class", "btn btn-common");
      cancelDeleteButton.setAttribute("onclick", "cancelDelete()");
      cancelDeleteButton.innerHTML = "Cancel";
      mainbtn.appendChild(cancelDeleteButton);
  }
  
  
  function cancelDelete() {
      var deleteData = document.getElementById('deletesatbtn');
      deleteData.disabled = false;
      var mainbtn = document.getElementById('whateverdiv');
      mainbtn.innerHTML = "";
      mainbtn.style.display = "none";
  }
  
  function imageFieldDelete() {
      checkedboxes = [];
      j = 0;
      var checkboxes = document.getElementsByName('satbtnchecks');
      for (i = 0; i < checkboxes.length; i++) {
  
          if (checkboxes[i].checked == true) {
              checkedboxes[j] = checkboxes[i].value;
              j++;
          }
      }
      requeststring = ""
      for (i = 0; i < checkedboxes.length; i++) {
          if (i != 0) {
              requeststring += ",'" + checkedboxes[i] + "'";
          } else {
              requeststring += "'" + checkedboxes[i] + "'";
          }
      }
      if (checkedboxes.length == 0) {
          alert('Please select single date or multiple dates to delete');
      } else {
          finalKill(requeststring);
      }
  
  
  }
  
  function finalKill(requestString) {
      $("#dialog-confirm-image").dialog({
          resizable: false,
          height: "auto",
          width: 400,
          modal: true,
          buttons: [{
                  text: 'Yes, delete field data',
                  class: 'btn-danger',
                  click: function() {
                      var x = new XMLHttpRequest();
                    var data={"farmID":farmID,"sat_time":requestString};
                      x.open("POST", "./deleteimagefield.php?fieldid=", true);
                      x.setRequestHeader('Content-Type',"application/json;charset=UTF-8");
                      x.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) {
                              var result = this.responseText;
                              if (result.includes("success")) {
                                  $("#dialog-confirm-image").dialog("close");
                                  alert("Farm data from " + farmID + " deleted");
                              } else {
                                  $("#dialog-confirm-image").dialog("close");
                                  alert("Unable to delete farm data from " + farmID);
                              }
                               $("#akashList").empty();
                             //   $("#timelines").hide(); //--hide today
                              fetchresults(farmID);
                              cancelDelete();
                          }
                      }
                      x.send(JSON.stringify(data));
                  }
              },
              {
                  text: 'No, cancel',
                  class: 'btn-default',
                  click: function() {
                      $(this).dialog("close");
                  }
              }
          ]
      });
  
  }
  
  
  function figuresPrepare(fieldbutton) {
      var abcd = fieldbutton;
      var abc = abcd.split("/");
      var str = abc[2] + "-" + abc[1] + "-" + abc[0];
      var fieldDateButtonPrepare = document.createElement("option");
      currentDate = fieldDateButtonPrepare;
      fieldDateButtonPrepare.name = farmID;
      fieldDateButtonPrepare.innerHTML = str;
      figures(fieldDateButtonPrepare);
  
  }
  
  function geoForweather() {
      var geom = JSON.parse(geometry);
      var length = geom.features[0].geometry.coordinates[0].length;
      var centroid = new Array();
      var centSubject = geom.features[0].geometry.coordinates[0];
      var a = 0;
      var b = 0;
      for (i = 0; i < length; i++) {
          a = a + centSubject[i][0];
          b = b + centSubject[i][1];
      }
      centroid[0] = a / length;
      centroid[1] = b / length;
      return centroid;
  
  }
  
  function prepareWeatherCall() {
      readKey();
      var centroid = geoForweather();
      var url = "https://api.openweathermap.org/data/2.5/weather?lat=" + centroid[1] + "&lon=" + centroid[0] + "&appid=" + apiid;
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url, true);
      xhr.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              var weatherData = this.responseText;
              weatherAction(weatherData);
          }
      }
      xhr.send();
  }
  
  function addDays(date, days) {
      var result = new Date(date);
      result.setDate(result.getDate() + days);
      if (result.getMonth() + 1 < 10) { var month = "0" + (result.getMonth() + 1); } else { var month = result.getMonth() + 1; }
      if (result.getDate() < 10) { var day = "0" + result.getDate(); } else { var day = result.getDate(); }
      return result.getFullYear() + "-" + month + "-" + day;
  }
  
  function readKey() {
      var rawFile = new XMLHttpRequest();
      rawFile.open("GET", "weatherData.txt", true);
      rawFile.onreadystatechange = function() {
          if (rawFile.readyState == 4) {
              var allText = rawFile.responseText;
              keepKey(allText);
              // document.getElementById("textSection").innerHTML = allText;
          }
  
      }
      rawFile.send();
  }
  
  function keepKey(key) {
      apiid = key;
  }
  
  function weatherAction(weatherData) {
      var d=new Date(0);
      var data = JSON.parse(weatherData);
      d.setUTCSeconds(data.dt+data.timezone+d.getTimezoneOffset()*60);
      
      var hour=d.getHours() > 9 ? d.getHours() :"0"+d.getHours();
      var minutes=d.getMinutes()>9 ?d.getMinutes():"0"+d.getMinutes();
      var time=hour+":"+minutes;
      var temp= data.main.temp-273.15 > 0 ? "+"+ Math.round((data.main.temp-273.15)*10)/10 +"°":Math.round((data.main.temp-273.15)*10)/10 +"°";
    
  
      document.getElementById("weatherTime").innerHTML=time;
      document.getElementById("humidity").innerHTML=data.main.humidity+" %";  
      document.getElementById("cloudCover").innerHTML=data.clouds.all+" %";  
      document.getElementById("temperature").innerHTML=temp;  
      
      var address="./assets/images/weathericons/"+data.weather[0].icon+".svg";
      var desc=data.weather[0].description;
      
      document.getElementById("iconWeather").src=address;
      document.getElementById("descWeather").innerHTML=desc;
      
  }
  
   
  function compare(a,b){
      if( a.dateCreate>b.dateCreate) return 1;
      else return -1;
  }
  
  function compareSowingDate(a,b){
      if( a.sowingDate>b.sowingDate) return 1;
      else return -1;
  }
  
  function compareArea(a,b){
      if( a.area>b.area) return 1;
      else return -1;
  }
  
  function compareRev(a,b){
      if( a.dateCreate>b.dateCreate) return -1;
      else return 1;
  }
  
  function compareSowingDateRev(a,b){
      if( a.sowingDate>b.sowingDate) return -1;
      else return 1;
  }
  
  function compareAreaRev(a,b){
      if( a.area>b.area) return -1;
      else return 1;
  }
  
  var sorter =[compare,compareRev,compareArea,compareAreaRev,compareSowingDate,compareSowingDateRev];
  

  function loadCentroids(farms){
    try{
        map.removeLayer(markers);
    }
    catch{
        console.log("no markers");
    };

      var farmPoints = [];
    for(i=0; i<farms.length; i++){
        var latitude = farms[i].latitude;
        var longitude = farms[i].longitude;
        var croptype = farms[i].crop;
        var sowingDate = farms[i].sowingDate;
        var area = farms[i].area;
        var fieldname = farms[i].fieldid;
        if(fieldname!=farmID){
            farmPoints.push([latitude, longitude, croptype, sowingDate, area, fieldname]);
        }
        
    }
    markers = L.markerClusterGroup();
		
		for (var i = 0; i < farmPoints.length; i++) {
			var a = farmPoints[i];
            var title = a[5];
            var ar = a[4];
			var marker = L.marker(new L.LatLng(a[0], a[1]), { title: title });
            marker.bindPopup('<p class="instruction">'+title+'</p><p>Crop: '+ a[2] + '</p><p>Area: '+ ar + '</p><p>Sowing Date: '+ a[3] + 
            '</p><button class="field" style="background-color:var(--primary-color) !important; padding:3px !important; color: white !important; border-radius:3px !important; border:none !important;" name="'+title+'">View crop health</button>');
			markers.addLayer(marker);
		}
        map.addLayer(markers);
  }
  
  function checkSD(date){
    var today= new Date();
    var sw= addDays(date,365);
    if(today>=new Date(sw)){
        alert('Sowing date is older than one year, you will get updates from last 12 Months');
        return addDays(today.getTime(),-365);
    }
    
    else return date;
  }

  function mouser(d){
    formatDate = d3.timeFormat("%b %d %Y");
      hover.style.display="block";
      d3.select(this).attr("fill" ,"#e62027").attr("stroke","#e62027").attr("r","6");
      hover.attr("fill","#ffffff");
      hover.append('text').html(formatDate(d.date)).attr("fill","#000000");
      hover.append('text').html("NDVI: "+d.value).attr("y","-1em").attr("fill","#000000");
      hover.attr("transform","translate("+(x1(d.date)-10)+","+(y1(d.value)-20)+")");
      document.getElementsByClassName('hovertip')[0].style.zIndex=1000;
  }
  function mouse(){
      hover.selectAll('text').remove();
      d3.select(this)
      .attr("stroke-width", "2px").attr("fill" ,"#fff").attr("stroke","#e62027").attr("r","3");

  }
 // INFO hml code copied here onwards

 $('#editFarm').click(function(){
    farmEdit(farmID);
});

// FIXME uncomment editfarm in index to use this

function farmEdit(farmID){
    let i =0;
    while (farmID!=farms[i].fieldid){
        i++;
    }
    console.log(farms[i]);
    $("#editCropInput").val(farms[i].crop);
    $("#editLocation").val(farms[i].location);
    // $("#editSowingDate").val(farms[i].sowingDate);
    

    hideElements();
    $("#editFieldForm").show();

}
$('#saveEdit').click(function(){
    fieldEdit(farmID);
});

function fieldEdit(farmID) {
    /* function for creating new field in the database */
    var location = document.getElementById("editLocation");
    var crop = document.getElementById("editCropInput");
    var sowingDate = document.getElementById("editSowingdate");

    if (crop.value == '' || sowingDate.value == '' ) {
        $('#editSubmitStatus').html("Please enter all details and then save changes");
    } else {
        $('#editSubmitStatus').html("");
      sowingDate.value=checkSD(sowingDate.value);
        data = "location=" + location.value + "& crop=" + crop.value + "& sowingdate=" + sowingDate.value + "& fieldId=" + farmID;
        $.post('./editField.php', data, function(result) {
            alert(result);
            showAddedFields();
            loadCentroids(farms);
        });

        setTimeout(displaySwitchBack, 1000);
    }
}

$('#cancelEditField').click(function(){
    setTimeout(displaySwitchBack, 200);
});


function addDropdown(){

    a = document.getElementById('dropdownDiv');
    ndvi = document.createElement("input");
    ndvi.type='radio';
    ndvi.id = "ndvi";
    ndvi.value="NDVI";
    ndvi.name="insightSelector";
    ndvi.setAttribute("checked","");
    ndviLabel = document.createElement("label");
    ndviLabel.innerHTML="NDVI";
    ndviLabel.htmlFor="ndvi";

    ndwi = document.createElement("input");
    ndwi.type='radio';
    ndwi.id = "ndwi";
    ndwi.value="NDWI";
    ndwi.name="insightSelector";
    ndwiLabel = document.createElement("label");
    ndwiLabel.innerHTML="NDMI";
    ndwiLabel.htmlFor="ndwi";

    truecolor = document.createElement("input");
    truecolor.type='radio';
    truecolor.id = "truecolor";
    truecolor.value="TRUE";
    truecolor.name="insightSelector";
    truecolorLabel = document.createElement("label");
    truecolorLabel.innerHTML="RGB image";
    truecolorLabel.htmlFor="truecolor";

    baseMap = document.createElement("input");
    baseMap.type='checkbox';
    baseMap.id = "baseMap"
    baseMap.value="Null"
    baseMap.name="basemapSelector";
    baseLabel = document.createElement("label");
    baseLabel.innerHTML="Hide image on map";
    baseLabel.htmlFor="baseMap";
    
    ndviDiv = document.createElement("div");
    ndviDiv.className="col-3";
    ndviDiv.append(ndvi,ndviLabel);

    ndwiDiv = document.createElement("div");
    ndwiDiv.className="col-3";
    ndwiDiv.append(ndwi,ndwiLabel);

    truecolorDiv = document.createElement("div");
    truecolorDiv.className="col-3";
    truecolorDiv.append(truecolor,truecolorLabel);

    basemapDiv = document.createElement("div");
    basemapDiv.className="col-3";
    basemapDiv.append(baseMap, baseLabel);

  a.append(ndviDiv,ndwiDiv,truecolorDiv,basemapDiv);

  b=document.getElementsByName("insightSelector");
  for(i=0;i<b.length;i++){
      b[i].onclick= function(){insightChange(this);};
    }
}
addDropdown();

$('#baseMap').click(function(){
    
    if($(this). is(":checked")){
        console.log("checked");
        removeExistingImages();
    }
    else{
        console.log("Not checked");
        if(insightImage[insightType]){
            drawImageonMap(insightImage[insightType],geometry);
        }  
    }
});

function makeTimelineGlobal(data){
    timelineData =data;

}

function checkRadios(data,insightType){
    // document.getElementById(insightType.toLowerCase()).checked=false;
    insightType=data[insightType]=="" || data[insightType]==null?"NDVI":insightType;
    document.getElementById(insightType.toLowerCase()).click();
    console.log(data);
    var noNDWI = data['NDWI']=="" || data['NDWI']==null;
    var noTRUE = data['TRUE']=="" || data['TRUE']==null;
    console.log(noTRUE, noNDWI);
    $("input[type=radio][value='NDWI']").prop("disabled",noNDWI);
    $("input[type=radio][value='TRUE']").prop("disabled",noTRUE);
    // console.log(insightType);
    return insightType;

    
}

function downloadShape(){
  // json = {url: "https://monitor.agriforetell.com/api/images/SHTestshpFarm_03062020_1_54_52.svg", index : "ndvi"};
  json = {url:insightImage[insightType],index:insightType.toLowerCase()};
  var data = JSON.stringify(json);
  var currentDatestr = currentDate.innerHTML;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;


  xhr.addEventListener("readystatechange", function() {
    if(xhr.readyState == 4 && xhr.status == 200){
      // console.log(xhr.responseText);
      var response = JSON.parse(xhr.responseText);
      console.log(response);
      var message = JSON.parse(response);
      console.log(message);
      if (message["status"] == "error"){
        alert(message["message"]);
      }else{              
        var blob = new Blob([response]);
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        if (insightType == "NDWI"){
          insightType = "NDMI";
          link.download = farmID+"_"+currentDatestr+"_"+insightType+".geojson";
        } else{
          link.download = farmID+"_"+currentDatestr+"_"+insightType+".geojson";
        }
        link.click();
      }

    }
  });

  xhr.open("POST", "dwshpfle.php");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.setRequestHeader("Access-Control-Allow-Origin", '*');
  xhr.send(data);
 
  // var downloadBtn = document.getElementById("downloadBtn");
  // var index = insightType;
  // var currentDatestr = currentDate.innerHTML;
  // json = {fieldname:{farmID}, date:{currentDatestr}, index:{insightType}, url:{insightImage}};
  // json = {index, url:"https://monitor.agriforetell.com/api/images/39_Field15_08092020_1.svg"};
  // json = {url: "https://monitor.agriforetell.com/api/images/SHTestshpFarm_03062020_1_54_52.svg", index : "ndvi"};
  // var Url = "https://us-central1-iron-rex-264011.cloudfunctions.net/shapefiledownload-staging";
  // var Url = "https://httpbin.org/post";
  // var xhr = new XMLHttpRequest();
  // xhr.open('POST', Url, true);
  // xhr.setRequestHeader('Content-Type', 'application/json');
  // xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
  // xhr.send(json);
  // xhr.onreadystatechange = processRequest;
  // function processRequest(e) {
  //   if (xhr.readyState == 4 && xhr.status == 200) {
  //     var response1 = JSON.parse(xhr.responseText);
  //     console.log(this.responseText);
  //     var shapefile = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json));
  //     var blob = new Blob([JSON.stringify(json)]);
  //     var link = document.createElement('a');
  //     link.href = window.URL.createObjectURL(blob);
  //     link.download = "shapefile.json";
  //     link.click();
  //   }
  // }
  console.log(json);
}