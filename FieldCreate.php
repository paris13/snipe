<?php 

if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}

/* this php file helps in creating a new field for the registerd user */
session_start();    
include "./sqlconfig.php";
    //$username=$_SESSION['name'];
    // echo(var_dump($_POST));
    $farmName = $_POST['farmName'];
    $location = $_POST['location'];
    $crop = $_POST['crop'];
    $sowingdate = $_POST['sowingdate'];
    $geometry = $_POST['geometry'];
    $area = $_POST['area'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $username = $_SESSION['name'];
    $queryFieldEnquire = 
    "select fcount,user_id from user_info where user_id in (select user_id from user_credentials where user_name=?) ";
    $stmt=$con->prepare($queryFieldEnquire);

    $stmt->bind_param('s',$username); 

    $stmt->execute();
    $stmt->bind_result($rowCount,$uid);
    $stmt->fetch();
    $stmt->free_result();
    $newRow=$rowCount+1;
    $newvar=(string)$uid."_Field".(string)$newRow;
    // echo $newvar;


    function insertUserFields($uid,$newFieldId,$geom,$crop,$sowingDate,$location,$area,$latitude, $longitude,$con,$fcount,$farmName){
        $insert="insert into fields (user_id, field_id, geo, crop, sowing_date, location, acreage, latitude, longitude,farm_name) values (?,?,?,?,?,?,?,?,?,?)";
        $stmt=$con->prepare($insert);
        $stmt->bind_param("ssssssssss",$uid,$newFieldId,$geom,$crop,$sowingDate,$location,$area,$latitude, $longitude,$farmName);
        $stmt->execute();
        $stmt->free_result();
        $update ="update user_info set fcount=? where user_id=?";
        $stmt=$con->prepare($update);
        $stmt->bind_param("ss",$fcount,$uid);
        $stmt->execute();
    }

    insertUserFields($uid,$newvar,$geometry,$crop,$sowingdate,$location,$area,$latitude, $longitude,$con,$newRow,$farmName) ;
    echo ($farmName." created");

?>