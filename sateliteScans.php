<?php
if (!isset($_SERVER['HTTP_REFERER'])) {
    echo 'Unauthorized Access';
    exit();
}
session_start();
include "./sqlconfig.php";
/* selects the total number of temporal analysis data for a certain field */

$field_id = $_REQUEST['fieldId'];
$fetcher =
    "select sat_time from field_image where field_id=?";

$stmt = $con->prepare($fetcher);
$stmt->bind_param("s", $field_id);
$stmt->execute();
$stmt->bind_result($temp);
$resultset = array();

$i = 0;

while ($stmt->fetch()) {
    $resultset[$i] = $temp;
    $i++;
}

echo json_encode($resultset);
