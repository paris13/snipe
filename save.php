<?php 

// if(!isset($_SERVER['HTTP_REFERER']))
// {     echo 'Unauthorized Access'; 
//     exit();
// }

/* this php file helps in creating a new field for the registerd user */
    session_start();
    include "./sqlconfig.php";
    
    header("Content-Type: application/json; charset=UTF-8");
    $obj = json_decode($_POST["jsondata"],true);
    $farmID = $_POST["farmID"];
    // echo($_POST["jsondata"]);
    foreach ($obj['images'] as $img){
        $url = $img['url'];
        $rndvi = $img['values']['red'];
        $gndvi = $img['values']['green'];
        $yndvi = $img['values']['yellow'];
        $ndvi = $img['values']['average'];
        $input = str_replace('/', '-', $img['date']);
        $t = strtotime($input);
        $date = date('Y-m-d G:i:s',$t);
        insertImageData($farmID,$date,$url,$ndvi,$gndvi,$yndvi,$rndvi,$con);
    }
    
    function insertImageData($farmID,$date,$url,$ndvi,$gndvi,$yndvi,$rndvi,$con){
        
        $insert="insert into field_image (field_id, sat_time, imageurl, NDVI, gndvi, yndvi, rndvi) values (?,?,?,?,?,?,?)";
        $stmt=$con->prepare($insert);
        $stmt->bind_param("sssssss",$farmID,$date,$url,$ndvi,$gndvi,$yndvi,$rndvi);
        $stmt->execute();
        // $stmt->free_result();
    }

    echo("Saved to DB");
