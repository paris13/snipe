<?php

if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}

session_start();
include "./sqlconfig.php";

$fieldID=$_GET['field_id'];

$query="SELECT field_id, sat_time from field_image where sat_time 
in(select max(sat_time) from field_image where field_id =?)";

$stmt=$con->prepare($query);
$stmt->bind_param('s',$fieldID);
$stmt->execute();
$stmt->bind_result($fieldid,$maxtime);
$stmt->fetch();
$arr=array();
$arr[0]=$fieldid;
$arr[1]=$maxtime;

echo json_encode($arr);
