<?php 
if(!isset($_SERVER['HTTP_REFERER']))
{     
    // echo 'Unauthorized Access'; 
    echo "<script>window.location='./registration.php'</script>";
}
  session_start();
  include "sqlconfig.php";  
//   if(isset($_SESSION['payment_status'])){
//   if($_SESSION["payment_status"]  == "complete"){
//     echo "<script>window.location='./index.php'</script>";
//   }

//   else if($_SESSION["payment_status"] == "pending"){
//     echo "<script>window.location='./payment.php'</script>";
//   }
// }
// else{
//   echo "<script>window.location='./registration.php'</script>";
//   exit();
// }
?>

<html>


<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Create a stylish landing page for your business startup and get leads for the offered services with this free HTML landing page template.">
    <meta name="author" content="IgnisNova Robotics">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
  <meta property="og:site_name" content="" /> <!-- website name -->
  <meta property="og:site" content="" /> <!-- website link -->
  <meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
  <meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
  <meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
  <meta property="og:url" content="" /> <!-- where do you want your post to link to -->
  <meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>AgriForetell - Data driven agri-intelligence</title>

    <!-- from registration user -->
    <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="stylesheet" href="./assets/css/responsive.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKCXGV0LF4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'G-TKCXGV0LF4');
    </script>
    <!-- it ends here -->
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
  <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/form.css" rel="stylesheet">
    <!-- Icon -->
    <link rel="stylesheet" href="fonts/line-icons.css">
  
  <!-- Favicon  -->
    <link rel="icon" href="images/aflogo.ico">
    <style>
    .video-container {
        min-height: 100vh;
        /* width: 99vw; */
        overflow: hidden;
        position: relative;
    }

    body {
        overflow-x: hidden;
    }
    .video-container .overlay {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 1;
        background: white;
        opacity: 0.5;
    }
    video {
      min-width: 100%;
      min-height: 100%;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translateX(-50%) translateY(-50%);
    }
  </style>
  </head>
<body>
<!-- <form>
    <input type="textbox" name="name" id="name" placeholder="Enter your name"/><br/><br/>
    <input type="textbox" name="amt" id="amt" placeholder="Enter amt"/><br/><br/>
    <input type="button" name="btn" id="btn" value="Pay Now" onclick="pay_now()"/>
</form> -->
<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
  <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="registration.php"><img src="https://www.agriforetell.com/assets/img/aflogo2.png" alt="alternative"></a>
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#featurediv">Features</a>
                </li>
                <!--<li class="nav-item">-->
                <!--  <a class="nav-link" href="./loggedout.php"><i class="fas fa-sign-out-alt"></i> Logout</a>-->
                <!--</li>-->
                
                <!-- <li class="nav-item">
                    <a class="nav-link page-scroll" href="#request">Request</a>
                </li> -->

                <!-- Dropdown Menu -->          
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle page-scroll" href="#about" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">About</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="terms-conditions.html"><span class="item-text">Terms Conditions</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                        <a class="dropdown-item" href="privacy-policy.html"><span class="item-text">Privacy Policy</span></a>
                    </div>
                </li> -->
                <!-- end of dropdown menu -->

              <!--   <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">Subscribe</a>
                </li> -->
            </ul>
            <span class="nav-item social-icons">
                <span class="fa-stack">
                    <a target="new" href="https://www.facebook.com/ignisnovarobotics" >
                        <i class="fas fa-circle fa-stack-2x facebook"></i>
                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                    </a>
                </span>
                <span class="fa-stack">
                    <a target="new" href="https://www.linkedin.com/company/ignisnova-robotics/about/">
                        <i class="fas fa-circle fa-stack-2x linkedin"></i>
                        <i class="fab fa-linkedin-in fa-stack-1x"></i>
                    </a>
                </span>
            </span>
        </div>
    </nav>



 <header id="header" class="header video-container">
        <!-- <div class="overlay"></div> -->
                    <video autoplay muted loop>
                        <source src="images/v3_b.webm" type="video/mp4">
                    </video>
        <div class="header-content">
            <div class="container">
                <div>
                    <div class="col-lg-6 col-md-12 col-sm-12 mx-auto">
   
     <div class = "col-lg-12 col-md-12 col-sm-12 form-holder" id="registerForm" class="form-group">
      <!--<div class="form-data" style="background: #50C878;margin-bottom: 10px;padding: 3px;color:#ffffff; font-size: 12px;" >-->
      <!--  Early bird pricing for first 100 users-->
      <!--</div>-->
            <h4 class="form-header">1 year access to 13 accounts (Prof. Matsumura) for Agriforetell Snipe</h4>
        <br>
        <!--<input type="radio" id="student" name="accType" value="Student account" checked onclick="student()">-->
        <!--<label class="col-4.5 mx-auto" for="student">Student account</label>&nbsp;&nbsp;-->
        <!--<input type="radio" id="regular" name="accType" value="Regular accoun" onclick="student()">-->
        <!--<label class="col-4.5 mx-auto" id="regular" for="regular">Regular account</label><br>-->

        <!--<form class="row" id="studentDiv" style="width: 100%;margin-top: 10px;" method="post" enctype="multipart/form-data">        -->
            <!-- <input class="form-control col-5 mx-auto" id="fname" type="text" name="fname" placeholder="First Name" required>
        <!--    <input class="form-control col-5 mx-auto" id="lname" type="text" name="lname" placeholder="Last Name" required> -->
            <!--<input class="form-control col-11 mx-auto" id="username" type="text" name="username" value="<?php// echo $_SESSION["name"] ?>" disabled required>-->
            <!--<input class="user_info form-control col-11 mx-auto " id="email" type="text" name="email" value="<?php //echo $_SESSION["email"] ?>" disabled required>-->
          <!--   <input class="form-control col-11 mx-auto" id="pass" type="password" name="pass" placeholder="Password (minimum 8 characters)" required>
        <!--    <input class="form-control col-11 mx-auto" id="cpass" type="password" name="cpass" placeholder="Confirm password"> -->
            
            <!-- <input class="user_info form-control col-11 mx-auto" id="phone" type="text" name="phone" placeholder="Phone number"required> -->
        <!--    <span class="mx-auto align-left">Please upload your Univeristy ID</span><br>-->
        <!--    <input class="form-control col-11 mx-auto" type="file" name="file" id="file" required><br><br><br>-->
            <!-- <input class="btn btn-common  col-5 mx-auto" type="button" id="paybtn" name="paybtn" value="Pay 7 USD" onclick="pay_now7()"> -->
        <!--    <button class="btn btn-common col-lg-6 col-md-8 mx-auto">Pay&nbsp;<strike>40 USD</strike>&nbsp;&nbsp;&nbsp; <B>7 USD</B></button><br><br>-->
        <!--</form>        -->
        <!--<br>-->
        <form class="row" id="regular" method='post'>        
            <!-- <input class="form-control col-11 mx-auto" id="rfname" type="text" name="fname" placeholder="First Name"required>
            <input class="form-control col-11 mx-auto" id="rlname" type="text" name="lname" placeholder="Last Name" required> -->
            <!--<input class="form-control col-11 mx-auto" id="rusername" type="text" name="username" value="matsumuralab accounts access" disabled required>-->
            <!--<input class="user_info form-control col-11 mx-auto " id="remail" type="text" name="email" value="matsumuralab1@agriforetell.com" disabled required>-->
            <!-- <input class="form-control col-11 mx-auto" id="rpass" type="password" name="pass" placeholder="Password (minimum 8 characters)"required>
            <input class="form-control col-11 mx-auto" id="rcpass" type="password" name="cpass" placeholder="Confirm password"> -->
            
           <!--  <input class="user_info form-control col-11 mx-auto" id="rphone" type="text" name="phone" placeholder="Phone number"required> -->
            <br><br><br>
           <!--  <input class="btn btn-common  col-5 mx-auto" type="button" id="paybtn10" name="paybtn" value="Pay 10 USD" onclick="pay_now10()"> -->
           <button class="btn btn-common col-lg-6 col-md-8 mx-auto">Pay&nbsp; <B>91 USD</B></button><br>
           
        
        </form>
  <div style="font-size=8px;"> * Once you confirm the payment you will get all accounts information on the mail.</div>
</div>
</div>

<!-- <div id="dialog-forgot" class="vhcenter" title="Forgot password">
    <div class="row">
       <div class="p10 col-12" id="forgotDiv">
       <p class="cheerful">No worries, just enter your username or email ID and we will send you an OTP to help reset your password.</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="usernameforgot" type="text" name="usernameforgot" placeholder="Username or E-Mail">
       </div>
       <div class="col-12 p10">
       <button id="sendOTP" class = "btn btn-common">Send OTP on registered E-mail</button>
       </div>       
       </div>
       
       <div class="p10 col-12" id="otpDiv">
       <p class="cheerful">Enter the OTP sent on your E-mail address</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="otp" type="text" name="otp" placeholder="Enter OTP">
       </div>
       <div class="col-12 p10">
       <button id="confirmOTP" class = "btn btn-common">Confirm OTP and reset password</button>
       </div>
       </div>

       <div class="p10 col-12" id="resetDiv">
       <p class="cheerful">Reset password</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="newpass" type="password" name="newpass" placeholder="Enter new password">
       <input class="form-control" id="newpassconfirm" type="password" name="newpassconfirm" placeholder="Confirm new password">
       </div>
       <div style="padding-top:5px;" class="col-12 plr5">
       <button id="resetPassword" class = "btn btn-common">Reset password</button>
       </div>
       </div>


       <div class="plr5 col-12">
       <p class="cheerful">Need help? <a class="primarylink" target="new" href="https://www.ignisnova.com#contact">Contact us</a></p>
       </div>
       
    </div>
</div> -->

                    
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header>
    <div id="featurediv" class="containerdiv altbg">
        <!-- Features Section Start -->
    <section id="features" class="section-padding bg-gray">
        <div class="container containerdiv">
          <div class="section-header text-center">
            <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Features</h2>
            <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="content-left">
                <div class="box-item wow fadeInLeft" data-wow-delay="0.3s">
                  <span class="icon">
                    <i class="lni-rocket"></i>
                  </span>
                  <div class="text">
                    <h6> Remote monitoring of your farm from anywhere in the world</h6>
                    <!-- <p>Proprietary AI algorithms and data processing APIs for fast and accurate results </p> -->
                  </div>
                </div>
                <div class="box-item wow fadeInLeft" data-wow-delay="0.6s">
                  <span class="icon">
                    <i class="lni-laptop-phone"></i>
                  </span>
                  <div class="text">
                    <h6>Near real-time insights updated every 5-7 days</h6>
                  </div>
                </div>
                <div class="box-item wow fadeInLeft" data-wow-delay="0.9s">
                  <span class="icon">
                    <i class="lni-cloud"></i>
                  </span>
                  <div class="text">
                    <h6>Current weather condition at the farms</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="show-box wow fadeInUp" data-wow-delay="0.3s">
                <img src="images/dashboard.png" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="content-right">
                <div class="box-item wow fadeInRight" data-wow-delay="0.3s">
                  <span class="icon">
                    <i class="lni-layers"></i>
                  </span>
                  <div class="text">
                    <h6>Monitor multiple farms from a single dashboard</h6>
  
                  </div>
                </div>
                <div class="box-item wow fadeInRight" data-wow-delay="0.6s">
                  <span class="icon">
                    <i class="lni-bar-chart"></i>
                  </span>
                  <div class="text">
                    <h6>Growth trend graphs and exporting data to Excel sheets</h6>
  
                  </div>
                </div>
                <div class="box-item wow fadeInRight" data-wow-delay="0.9s">
                  <span class="icon">
                    <i class="lni-leaf"></i>
                  </span>
                  <div class="text">
                    <h6>Quantitative data of crop growth for taking informed decisions</h6>
                    <!-- <p></p> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Features Section End -->
    </div>
 <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>About Agriforetell</h4>
                        <p>Agriforetell is a platform that uses AI and Remote Sensing for enabling data-driven decision making in agriculture</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col middle">
                        <h4>Important Links</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">A product of <a class="turquoise" target="new" href="https://www.ignisnova.com">IgnisNova Robotics</a></div>
                            </li>
                        </ul>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col last">
                        <h4>Social Media</h4>
                        <span class="fa-stack">
                            <a target="new" href="https://www.facebook.com/ignisnovarobotics">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a target="new" href="https://www.linkedin.com/company/ignisnova-robotics/about/">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </a>
                        </span>
                    </div> 
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<!-- <script src="./main.js"></script> -->

<script>
    $("form#regular").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
    console.log(formData);

    $.ajax({
        url: 'payment_process2.php',
        type: 'POST',
        data: formData,
        success: function (data) {
            console.log(data);
            var options = {
                        "key": "rzp_live_USL9rr9xVNsUvJ", 
                        "amount": 9100, 
                        "currency": "USD",
                        "name": "Agriforetell",
                        "description": "1 year access to Agriforetell Snipe",
                        "image": "https://www.agriforetell.com/assets/img/aflogo2.png",
                        "handler": function (response){                            
                           jQuery.ajax({
                               type:'post',
                               url:'payment_process2.php',
                               data:"payment_id="+response.razorpay_payment_id,
                               success:function(result){
                                   alert('payment successful');
                                  window.location.href="success.php";
                               },
                               error:function(){
                                        window.location.href="loggedout.php";
                                    }
                           });
                           console.log(response);
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();

        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!-- Scripts -->
<script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
<script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
<script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
<script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
<script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
<script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
<!-- <script src="js/browser.js"></script>  -->
<!-- Custom scripts -->
<script src="js/scripts.js"></script> <!-- Custom scripts -->
<!--<script src="./payment.js"></script>-->


</body>
</html>