<?php
session_start();
include "sqlconfig.php";

if (isset($_SESSION['name'])) {
    echo "<script>window.location='index.php'</script>";
    exit();
} else {
}
?>

<html>


<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Create a stylish landing page for your business startup and get leads for the offered services with this free HTML landing page template.">
    <meta name="author" content="IgnisNova Robotics">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="" /> <!-- website name -->
    <meta property="og:site" content="" /> <!-- website link -->
    <meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" /> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>AgriForetell - Data driven agri-intelligence</title>

    <!-- from registration user -->
    

    <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="stylesheet" href="./assets/css/responsive.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKCXGV0LF4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'G-TKCXGV0LF4');
    </script>
    <!-- it ends here -->
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/form.css" rel="stylesheet">
    <!-- Icon -->
    <link rel="stylesheet" href="fonts/line-icons.css">
    
    <!-- Favicon  -->
    <link rel="icon" href="images/aflogo.ico">
    <style>
.video-container {
    min-height: 100vh;
    /* width: 99vw; */
    overflow: hidden;
    position: relative;
}

body {
    overflow-x: hidden;
}
.video-container .overlay {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 1;
    background: white;
    opacity: 0.5;
}
video {
  min-width: 100%;
  min-height: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
}

</style>
</head>

<body>
   
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
    <div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="registration.php"><img src="https://www.agriforetell.com/assets/img/aflogo2.png" alt="alternative"></a>
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#featurediv">Features</a>
                </li>
                <!--<li class="nav-item">-->
                <!--    <a class="nav-link page-scroll" href="#pricing">Pricing</a>-->
                <!--</li>-->
                <!-- <li class="nav-item">
                    <a class="nav-link page-scroll" href="#request">Request</a>
                </li> -->

                <!-- Dropdown Menu -->          
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle page-scroll" href="#about" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">About</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="terms-conditions.html"><span class="item-text">Terms Conditions</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                        <a class="dropdown-item" href="privacy-policy.html"><span class="item-text">Privacy Policy</span></a>
                    </div>
                </li> -->
                <!-- end of dropdown menu -->

                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">Subscribe</a>
                </li>
            </ul>
            <span class="nav-item social-icons">
                <span class="fa-stack">
                    <a target="new" href="https://www.facebook.com/ignisnovarobotics">
                        <i class="fas fa-circle fa-stack-2x facebook"></i>
                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                    </a>
                </span>
                <span class="fa-stack">
                    <a target="new" href="https://www.linkedin.com/company/ignisnova-robotics/about/">
                        <i class="fas fa-circle fa-stack-2x linkedin"></i>
                        <i class="fab fa-linkedin-in fa-stack-1x"></i>
                    </a>
                </span>
            </span>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->

    

    <!-- Header -->
    <header id="header" class="header video-container">
        <!-- <div class="overlay"></div> -->
                    <video autoplay muted loop>
                        <source src="images/v3_b.webm" type="video/mp4">
                    </video>
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-container">
                            <h1><span class="headerhighlight">Monitor crop growth</span><br><span class="whitetext"> using satellite imagery </span></h1>
                            <p class="p-large whitetext">Get near-real time updates of your crop growth, every 5-7 days.</p>
                            <!-- <a class="btn-solid-lg page-scroll" href="#services">SIGN UP</a> &nbsp; &nbsp;<a class="btn-outline-lg page-scroll" href="#services">LOGIN</a> -->
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6 col-md-12 col-sm-12 mx-auto">
    <div class = "col-lg-8 col-md-12 col-sm-12 form-holder" id="loginForm" style="float:right;">
        <h2 class="form-header">Login</h2> 
    <br>
        <form class="form-group" method="POST" action="./login.php" >
                <input class="form-control" id="usernametext" type="text" name="username" placeholder="Username">
                <input class="form-control" id="passwordtext" type="password" name="pass" placeholder="Password">
            <br>
                <input class="btn btn-common" id="submitbutton" type="submit">
        </form>
    
        <div>
            <p>Don't have an account? <a class="textlink" id="signuplink">Sign up here</a></p>
            
            <a class="textlink" id="forgot" style="font-size:0.9em"> Forgot password</a>
            <!--<p><a href = "https://agriforetell.com/#contact" target="new">Contact us here</a> for a 14-day trial.</p>-->
        </div>
    </div>
    <div class = "col-lg-8 col-md-12 col-sm-12 form-holder" id="registerForm" class="form-group" style="float: right;display: none;">
            <h2 class="form-header">Register here</h2>
        <br>
    
        <div class="row">
        <input class="form-control col-5 mx-auto" id="fname" type="text" name="fname" placeholder="First name">
        <input style="display:none" class="form-control col-5 mx-auto" id="mname" type="text" name="mname" placeholder="middle name">
        <input class="form-control col-5 mx-auto" id="lname" type="text" name="lname" placeholder="Last name">    
    
        <input class="user_credentials form-control col-11 mx-auto" id="user_name" onkeyup="userCheck(this);" type="text" name="user_credentials" placeholder="Username">
        <input class="form-control col-11 mx-auto" id="pass" type="password" name="pass" placeholder="Password (minimum 8 characters)">
        <input class="form-control col-11 mx-auto" id="cpass" type="password" name="cpass" placeholder="Confirm password">
        <input class="user_info form-control col-5 mx-auto " id="email" onkeyup="userCheck(this);" type="text" name="user_info" placeholder="E-mail address">
        <input class="user_info form-control col-5 mx-auto" id="phone" onkeyup="userCheck(this);" type="text" name="user_info" placeholder="Phone number">
        <!-- <input id="submit" type="submit"> -->
    </div>
        <br>
        <button class="btn btn-common" onclick="dataSubmit()">Register</button>
    
    <br>
    
<div>
    <p>Already have an account? <a class="textlink" id="loginlink"> Login here</a></p>
</div>
</div>
</div>

<div id="dialog-forgot" class="vhcenter" title="Forgot password">
    <div class="row">
       <div class="p10 col-12" id="forgotDiv">
       <p class="cheerful">No worries, just enter your username or email ID and we will send you an OTP to help reset your password.</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="usernameforgot" type="text" name="usernameforgot" placeholder="Username or E-Mail">
       </div>
       <div class="col-12 p10">
       <button id="sendOTP" class = "btn btn-common">Send OTP on registered E-mail</button>
       </div>       
       </div>
       
       <div class="p10 col-12" id="otpDiv">
       <p class="cheerful">Enter the OTP sent on your E-mail address</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="otp" type="text" name="otp" placeholder="Enter OTP">
       </div>
       <div class="col-12 p10">
       <button id="confirmOTP" class = "btn btn-common">Confirm OTP and reset password</button>
       </div>
       </div>

       <div class="p10 col-12" id="resetDiv">
       <p class="cheerful">Reset password</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="newpass" type="password" name="newpass" placeholder="Enter new password">
       <input class="form-control" id="newpassconfirm" type="password" name="newpassconfirm" placeholder="Confirm new password">
       </div>
       <div style="padding-top:5px;" class="col-12 plr5">
       <button id="resetPassword" class = "btn btn-common">Reset password</button>
       </div>
       </div>


       <div class="plr5 col-12">
       <p class="cheerful">Need help? <a class="primarylink" target="new" href="https://www.ignisnova.com#contact">Contact us</a></p>
       </div>
       
    </div>
</div>

                    
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->




    <div>
    <h2 id="messageform"></h2>
    </div>


 <!-- Details 1 -->
 <div class="basic-1 cards-1">
        <div class="container">
                <div class="row">
                        <div class="col-lg-12">
                            <h2 class="turquoise">How does it work?</h2>
                            <p class="p-heading p-large">It's a simple process.</p>
                        </div> <!-- end of col -->
                    </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>1. Add your farm on the map</h2>
                        <p class="p-large">Draw the polygon around your farm, enter a few details about your crop (like crop type, sowing date) and save your farm</p>
                        <!-- <a class="btn-solid-reg popup-with-move-anim" href="#details-lightbox-1">LIGHTBOX</a> -->
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid rounded" src="images/polyselect2_edited.gif" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of details 1 -->
    
    <!-- Details 2 -->
    <div class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid rounded" src="images/timelinescroll_0_5x.gif" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>2. Get analysed satellite images of the crop</h2>
                        <p class="p-large">Scroll through the timeline to see crop growth analysis on every 5-7 days since the sowing date.</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of details 2 -->

    <!-- Details 3 -->
    <div class="basic-1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="container" style="text-align: center">
                            <h2>3. Take action</h2>
                            <p class="p-large">Based on the analysis and growth trend of the crops, <br> understand which regions of the farm need your attention</p>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <!-- <div class="col-lg-6">
                        <div class="image-container">
                            <img class="img-fluid rounded" src="images/timelinescroll_0_5x.gif" alt="alternative">
                        </div> 
                    </div>  -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of basic-1 -->
        <!-- end of details 3 -->

    </div>

    <!-- <div id="colorsdiv" class="containerdiv">

            <div class="basic-1 cards-1">
                <div class="container">
                        <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="turquoise">What do these colors denote?</h2>
                                    <p class="p-heading p-large">Understanding the analysed images.</p>
                                </div> 
                            </div> 
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="text-container">
                                <h2>1. Add your farm on the map</h2>
                                <p class="p-large">Draw the polygon around your farm, enter a few details about your crop (like crop type, sowing date) and save your farm</p>
                             
                            </div> 
                        </div> 
                        <div class="col-lg-6">
                            <div class="image-container">
                                <img class="img-fluid rounded" src="images/polyselect_1_5x.gif" alt="alternative">
                            </div>
                        </div> 
                    </div> 
                </div>
            </div> 
    </div> -->

    <div id="featurediv" class="containerdiv altbg">
        <!-- Features Section Start -->
    <section id="features" class="section-padding bg-gray">
        <div class="container containerdiv">
          <div class="section-header text-center">
            <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Features</h2>
            <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="content-left">
                <div class="box-item wow fadeInLeft" data-wow-delay="0.3s">
                  <span class="icon">
                    <i class="lni-rocket"></i>
                  </span>
                  <div class="text">
                    <h6> Remote monitoring of your farm from anywhere in the world</h6>
                    <!-- <p>Proprietary AI algorithms and data processing APIs for fast and accurate results </p> -->
                  </div>
                </div>
                <div class="box-item wow fadeInLeft" data-wow-delay="0.6s">
                  <span class="icon">
                    <i class="lni-laptop-phone"></i>
                  </span>
                  <div class="text">
                    <h6>Near real-time insights updated every 5-7 days</h6>
                  </div>
                </div>
                <div class="box-item wow fadeInLeft" data-wow-delay="0.9s">
                  <span class="icon">
                    <i class="lni-cloud"></i>
                  </span>
                  <div class="text">
                    <h6>Current weather condition at the farms</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="show-box wow fadeInUp" data-wow-delay="0.3s">
                <img src="images/dashboard.png" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="content-right">
                <div class="box-item wow fadeInRight" data-wow-delay="0.3s">
                  <span class="icon">
                    <i class="lni-layers"></i>
                  </span>
                  <div class="text">
                    <h6>Monitor multiple farms from a single dashboard</h6>
  
                  </div>
                </div>
                <div class="box-item wow fadeInRight" data-wow-delay="0.6s">
                  <span class="icon">
                    <i class="lni-bar-chart"></i>
                  </span>
                  <div class="text">
                    <h6>Growth trend graphs and exporting data to Excel sheets</h6>
  
                  </div>
                </div>
                <div class="box-item wow fadeInRight" data-wow-delay="0.9s">
                  <span class="icon">
                    <i class="lni-leaf"></i>
                  </span>
                  <div class="text">
                    <h6>Quantitative data of crop growth for taking informed decisions</h6>
                    <!-- <p></p> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Features Section End -->
    </div>



    <!-- Pricing -->
    <!--<div id="pricing" class="cards-2 ">-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-lg-12">-->
    <!--                <h2>30-day free trial </h2>-->
    <!--                <p class="p-heading p-large">Monitor upto 200 farms, of 500 acres each.</p>-->
    <!--            </div> -->
    <!--        </div> -->
            
    <!--    </div> -->
    <!--</div> -->
    <!-- end of pricing -->

    <!-- Subscribe -->
    <div id="contact" class="form-2 altbg">
        <div class="container">
            <div class="row ">
                <div class="col-lg-12 text-center">
                    <h2>Stay in the loop!</h2>
                    <p class="p-large">Subscribe to our latest  product updates, we are excited to let you know about what more we are building. <br> No spam. No third-parties involved.  </p>
                </div>
                <div class="col-lg-2"></div>
                <div class="col-lg-8 text-center" style="padding: 20px;">
                    <!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
    /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://agriforetell.us20.list-manage.com/subscribe/post?u=a7859969f686043e9dfe0e766&amp;id=395e9706d4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
    <!-- <h2>Subscribe</h2> -->
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
    <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
    <label for="mce-FNAME">First Name  <span class="asterisk">*</span>
</label>
    <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
    <label for="mce-LNAME">Last Name </label>
    <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
    <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a7859969f686043e9dfe0e766_395e9706d4" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
                </div>
                <div class="col-lg-2"></div> 
            </div> 

        </div>
    </div> 
    <!-- end of subscribe -->
   

    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>About Agriforetell</h4>
                        <p>Agriforetell is a platform that uses AI and Remote Sensing for enabling data-driven decision making in agriculture</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col middle">
                        <h4>Important Links</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">A product of <a class="turquoise" target="new" href="https://www.ignisnova.com">IgnisNova Robotics</a></div>
                            </li>
                            <!-- <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
                            </li> -->
                        </ul>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col last">
                        <h4>Social Media</h4>
                        <span class="fa-stack">
                            <a target="new" href="https://www.facebook.com/ignisnovarobotics">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a target="new" href="https://www.linkedin.com/company/ignisnova-robotics/about/">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </a>
                        </span>
                    </div> 
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->  
    <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © <a href="https://ignisnova.com">IgnisNova Robotics</a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!-- Scripts -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/browser.js"></script> <!-- Custom scripts -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
    <script src="./registration.js"></script>

</body>
</html>