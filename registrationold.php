<?php
session_start();
include "sqlconfig.php";

if (isset($_SESSION['name'])) {
    echo "<script>window.location='index.php'</script>";
} else {
}
?>

<html>
<style>

body {
    overflow-x: hidden;
}
</style>

<head>
    <title>
       Home page
    </title>
        <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="stylesheet" href="./assets/css/responsive.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKCXGV0LF4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'G-TKCXGV0LF4');
    </script>
</head>

<body>
    <div class="container-fluid" style="padding:0">
    <div class="row" id="form-container" style="margin:0px">
    <div class="col-12 mx-auto">
        <!-- <h1>AgriForetell</h1> -->
        <img id="aflogo" src="./assets/images/AFlogo.png">
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 mx-auto">
    <div class = "col-lg-8 col-md-12 col-sm-12 form-holder" id="loginForm" >
        <h2 class="form-header">Login</h2> 
    </br>
        <form class="form-group" method="POST" action="./login.php" >
                <input class="form-control" id="usernametext" type="text" name="username" placeholder="Username">
                <input class="form-control" id="passwordtext" type="password" name="pass" placeholder="Password">
            </br>
                <input class="btn btn-common" id="submitbutton" type="submit">
        </form>
    
        <div>
            <p>Don't have an account? <a class="textlink" id="signuplink">Sign up here</a></p>
            <a class="textlink" id="forgot" style="font-size:0.9em"> Forgot password</a>
        </div>
    </div>
    <div class = "col-lg-8 col-md-12 col-sm-12 form-holder" id="registerForm" class="form-group" style="display:none">
            <h2 class="form-header">Register here</h2>
        <br>
    
        <div class="row">
        <input class="form-control col-5 mx-auto" id="fname" type="text" name="fname" placeholder="First name">
        <input style="display:none" class="form-control col-5 mx-auto" id="mname" type="text" name="mname" placeholder="middle name">
        <input class="form-control col-5 mx-auto" id="lname" type="text" name="lname" placeholder="Last name">    
    
        <input class="user_credentials form-control col-11 mx-auto" id="user_name" onkeyup="userCheck(this);" type="text" name="user_credentials" placeholder="Username">
        <input class="form-control col-11 mx-auto" id="pass" type="password" name="pass" placeholder="Password (minimum 8 characters)">
        <input class="form-control col-11 mx-auto" id="cpass" type="password" name="cpass" placeholder="Confirm password">
        <input class="user_info form-control col-5 mx-auto " id="email" onkeyup="userCheck(this);" type="text" name="user_info" placeholder="E-mail address">
        <input class="user_info form-control col-5 mx-auto" id="phone" onkeyup="userCheck(this);" type="text" name="user_info" placeholder="Phone number">
        <!-- <input id="submit" type="submit"> -->
    </div>
        <br>
        <button class="btn btn-common" onclick="dataSubmit()">Register</button>
    
    <br>
    
<div>
    <p>Already have an account? <a class="textlink" id="loginlink"> Login here</a></p>
</div>
</div>
</div>
<div id="dialog-forgot" class="vhcenter" title="Forgot password">
       <div class="row">
       <div class="p10 col-12" id="forgotDiv">
       <p class="cheerful">No worries, just enter your username or email ID and we will send you an OTP to help reset your password.</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="usernameforgot" type="text" name="usernameforgot" placeholder="Username or E-Mail">
       </div>
       <div class="col-12 p10">
       <button id="sendOTP" class = "btn btn-common">Send OTP on registered E-mail</button>
       </div>       
       </div>
       
       <div class="p10 col-12" id="otpDiv">
       <p class="cheerful">Enter the OTP sent on your E-mail address</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="otp" type="text" name="otp" placeholder="Enter OTP">
       </div>
       <div class="col-12 p10">
       <button id="confirmOTP" class = "btn btn-common">Confirm OTP and reset password</button>
       </div>
       </div>

       <div class="p10 col-12" id="resetDiv">
       <p class="cheerful">Reset password</p>
       <div class = "col-12 p5lr">
       <input class="form-control" id="newpass" type="password" name="newpass" placeholder="Enter new password">
       <input class="form-control" id="newpassconfirm" type="password" name="newpassconfirm" placeholder="Confirm new password">
       </div>
       <div style="padding-top:5px;" class="col-12 plr5">
       <button id="resetPassword" class = "btn btn-common">Reset password</button>
       </div>
       </div>


       <div class="plr5 col-12">
       <p class="cheerful">Need help? <a class="primarylink" target="new" href="https://www.ignisnova.com#contact">Contact us</a></p>
       </div>
       
       </div>
    </div>
<div class="col-lg-6 col-md-12 col-sm-12 mx-auto" id="introimage">
</div>
<div class="col-12">
   <a href="https://www.ignisnova.com" target="blank"> <h6 class="bottomright">Powered by <img src="./assets/images/logo.png"></h6></a>
</div>
<div>
    <h2 id="messageform"></h2>
</div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="./registration.js"></script>
</body>
</html>