var newField=document.getElementById("newFieldRegistration");
var fields=document.getElementById("fieldsContainer");
var drawnItems, drawControlFull, drawControlEditOnly, drawnPolygon, area;

$("#saveField").click(function(){
    fieldCreate();
  });

  $("#cancelSaveField").click(function(){
    displaySwitchBack();
  });

function fieldCreate(){
    /* function for creating new field in the database */
    
    var fieldId = document.getElementById("fieldId");
    var location = document.getElementById("location");
    var crop = document.getElementById("cropInput");
    var farmName = document.getElementById("farmName");
    var sowingDate = document.getElementById("sowingdate");
    data="location="+location.value+"& crop="+crop.value+"& sowingdate="+sowingDate.value+"& farmName="+farmName.value;
    
    $.post('./FieldCreate.php', data, function(result) {    
        console.log(result);
    });
    
   setTimeout(displaySwitchBack,1000);
}

function Switch(){
    /* for switching from fields to create field*/
    removeExistingPolygons();
    startEditing();
    fields.style.display="none";
    newField.style.display="block";   
}

function displaySwitchBack(){
    /* opposite of switch function-> switches from create field to existing field window*/
    stopEditing();
    loadFields();
    newField.style.display="none";
    fields.style.display="block";
}

// Start editing function
    
// FeatureGroup is to store editable layers

function startEditing(){
drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);

drawControlFull = new L.Control.Draw({
    edit: {
        featureGroup: drawnItems
    },
    draw: {
        polyline: false,
        marker:false,
        circlemarker:false
    }
});


drawControlEditOnly = new L.Control.Draw({
    edit: {
        featureGroup: drawnItems
    },
    draw: false
});

map.addControl(drawControlFull);
map.on("draw:created", function (e) {
    var layer = e.layer;
    layer.addTo(drawnItems);
    drawControlFull.remove(map);
    drawControlEditOnly.addTo(map);
    drawnPolygon = layer.toGeoJSON();
    area = L.GeometryUtil.geodesicArea(layer.getLatLngs());
    console.log(area);
    console.log("Area");
});

map.on("draw:deleted", function(e) {
    check =  Object.keys(drawnItems._layers).length;
    console.log(check);
     if (check === 0){
         drawControlEditOnly.remove(map);
         drawControlFull.addTo(map);
     };
 });
};

// Stop editing

function stopEditing(){
    drawControlEditOnly.remove(map);
    drawControlFull.remove(map);
};