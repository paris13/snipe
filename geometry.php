<?php
// if(!isset($_SERVER['HTTP_REFERER']))
// {     echo 'Unauthorized Access'; 
//     exit();
// }

class Geometry{
    
    var $geometry;

    function setGeometry($geometry){
        $this->geometry=$geometry;
    }
    
}
/* fetches existing fields from field table for given user */
    session_start();
    include "./sqlconfig.php";
    
    $fetcher="select geo from fields where field_id=?";
    $stmt=$con->prepare($fetcher);
    $field_id=$_REQUEST['fieldId'];
    $stmt->bind_param("s", $field_id);
    $stmt->execute();
    $stmt->bind_result($temp);
    
    $resultset=array();
    $i=0;
    while($stmt->fetch())
    {   
        $fd=new Geometry();
        $fd->setGeometry($temp);
        $resultset[$i]=$fd;
        $i++;
    }
    
    echo json_encode($resultset);
