<?php
session_start();
include "./sqlconfig.php";
// $val = file_get_contents('php://input');
// saveDB ($val);
function saveDB ($val) {    
    include "./sqlconfig.php";
    // header("Content-Type: application/json; charset=UTF-8");
    $obj = json_decode($val,true);
    $farmID = $obj["farmID"];
    // echo($_POST["jsondata"]);
    foreach ($obj['images'] as $img){
        $url = $img['url'];
        $rndvi = $img['values']['red'];
        $gndvi = $img['values']['green'];
        $yndvi = $img['values']['yellow'];
        $ndvi = $img['values']['average'];
        
        try{
            $ndwiPath=$img['url_ndmi'];
            $ndwi=$img['values']['average_ndmi'];
        }
        catch(Exception $e){
            $ndwiPath=NULL;
            $ndwi=NULL;
        }
        try{
            $truePath=$img['url_true'];
        }
        catch(Exception $e){
            $truePath=NULL;
        }
        $input = str_replace('/', '-', $img['date']);
        $t = strtotime($input);
        $date = date('Y-m-d G:i:s',$t);
        echo($date);
        echo($url);
        echo($farmID);
        echo($ndvi);
        insertImageData($farmID,$date,$url,$ndvi,$gndvi,$yndvi,$rndvi,$ndwiPath,$ndwi,$truePath,$con);
    }
}
    
    function insertImageData($farmID,$date,$url,$ndvi,$gndvi,$yndvi,$rndvi,$ndwiPath,$ndwi,$truePath,$con){
        
       try{
        $insert="insert into field_image (field_id, sat_time, imageurl, NDVI, gndvi, yndvi, rndvi,ndwi_path,NDWI,true_path) values (?,?,?,?,?,?,?,?,?,?)";
        $stmt=$con->prepare($insert);
        $stmt->bind_param("ssssssssss",$farmID,$date,$url,$ndvi,$gndvi,$yndvi,$rndvi,$ndwiPath,$ndwi,$truePath);
        $stmt->execute();
        echo ("<br><center>saved to new database</center><br>");   
       }
       catch(Exception $e){
           echo("error in uploading to the database. Error: ".(str)$e);
       }
        
        
    }

    // echo("connected to DB");
