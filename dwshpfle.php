<?php
 $inputData = file_get_contents('php://input');
 $mydata = json_decode($inputData,true);
 $data = array("url"=>$mydata['url'] ,"index"=>$mydata['index']);
 $data = json_encode($data);

 function fetch($data){

    $url = "https://us-central1-iron-rex-264011.cloudfunctions.net/shapefiledownload-staging";
      
    // Initialize a CURL session. 
    $ch = curl_init(); 
      
    // Return Page contents. 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    //grab URL and pass it to the variable.
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,0); 
    curl_setopt($ch,CURLOPT_TIMEOUT,300);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST,true) ;
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json','Accept:application/json'));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    
      
    $result= curl_exec($ch);
    // $result=$result;
    curl_close ( $ch );
    return $result;  
}

//  echo fetch($data);
  echo json_encode(fetch($data));