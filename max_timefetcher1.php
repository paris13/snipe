<?php

if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}

session_start();
include "./sqlconfig.php";

$fieldID=$_GET['field_id'];
$queryfields="select field_id, sowing_date from fields where field_id =?";

$query="SELECT field_id, sat_time, imageurl from field_image where sat_time 
in(select max(sat_time) from field_image where field_id =?) and field_id =?";

$stmt=$con->prepare($query);
$stmt->bind_param('ss',$fieldID,$fieldID);
$stmt->execute();
$stmt->bind_result($fieldid,$maxtime,$imageurl);
$stmt->fetch();
$arr=array();
$arr[0]=$fieldid;
$arr[1]=$maxtime;
$arr[2]=$imageurl;

if($fieldID==null || $maxtime==null){
    $stmt=$con->prepare($queryfields);
    $stmt->bind_param('s',$fieldID);
    $stmt->execute();
    $stmt->bind_result($fieldid,$maxtime);
    $stmt->fetch();
    $arr=array();
    $arr[0]=$fieldid;
    $arr[1]=$maxtime;
    $arr[2]=$imageurl;
    echo json_encode($arr);
}
else{
    //echo var_dump($arr[1]);
    echo json_encode($arr);
}
