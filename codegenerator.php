<?php

function  randomCode(){
    // echo rand() ."\\n";
    // echo random_bytes(8) ."\\n";
    return rand(100000,999999) ;
}

function checkEmail($input){
    $regexEmail="/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i";
    $output=preg_match($regexEmail,$input);
    return $output;
}

function sendMail($email,$otpMail){
    
    ini_set("SMTP","mail.ignisnova.com");
    // Please specify an SMTP Number 25 and 8889 are valid SMTP Ports.
    ini_set("smtp_port","25");
    // Please specify the return address to use 
    ini_set('sendmail_from', 'contact@ignisnova.com');
    $recipient=$email;
    $senderEmail="support@agriforetell.com";
    $subject="Agriforetell account OTP";
    $sender="Agriforetell team";
    $message="Your OTP is $otpMail";
    $mailBody="$message \n\n Regards,\n$sender";

    if (mail($recipient, $subject, $mailBody, "From: $sender <$senderEmail>")){
        return true;
    } else {
        return false;
    }
}


?>