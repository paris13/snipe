<?php
if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}  

function saltGen (){
	  $salt  = "";
	  for ($i=0;$i<5;$i++){
	      $in =rand(50,122);
	      if ($in< 97){
	          $salt = $salt . (String) $in%9; 
	          
	      }
	      else $salt = $salt . chr($in);
	      
	  }
	 // echo ($salt."\n");
	  return $salt;
}

function insertusercredential($username,$password,$email,$con){
        // $con->refresh();
        $salt = saltGen();
        $hash = hash("sha512" , $password.$salt);
        $insert="insert into user_credentials (user_name,user_id,email,password,hash,salt ) values (?, (SELECT user_id from user_info where email =?),?,?,?,?)";
        $stmt=$con->prepare($insert);
        $stmt->bind_param('ssssss',$username,$email,$email,$password,$hash,$salt);
        $stmt->execute();
        $stmt->free_result();

    }
    

function insertuserinfo($con,$fname,$mname,$lname,$email,$phone){
    /*inserts a user information into user_info table when a new user is registered */
    //$con->refresh();
    $insert="insert into user_info (f_name,m_name,l_name,email,phone,fcount) values(?,?,?,?,?,?)";
    // $qUID="select count(*) as count from user_info";
    // $stmt=$con->query($qUID);
    // $val=$stmt->fetch_Object()->count;
    // $val++;
    $fid = 0;
    // $stmt->free_result();
    $stmt=$con->prepare($insert);
    $stmt->bind_param('ssssss',$fname,$mname,$lname,$email,$phone,$fid);
    $stmt->execute();
    $stmt->free_result();

}



function validateParam($table,$param,$value,$con){
    /* it verifies whether the parameter's value is true or not.
     it searches in different table depending upon the table that is is given   */
   // $con->refresh();
    $query="select ".$param." from " .$table." where " .$param. " = ?";
    $stmt=$con->prepare($query);
    $stmt->bind_param('s',$value);
    $stmt->execute();
    $stmt->store_result();
    $val=$stmt->num_rows;
    $stmt->free_result();
    return $val;
    
    
}

function checkValidPhone($regex,$pass,$cpass){
    /* checks whether the phone regex conditions are met or not*/
    if (preg_match($regex,$pass)==1){
        //echo "regex pass passed";
        if(strlen($pass)>=8 && strlen($pass)<=20 ){

            if($pass==$cpass){
                return true;
            }
            else return false;
        }
        else return false;

    }
    else return false;
}

function Valid($username,$email,$phone,$password,$cpassword,$fname,$mname,$lname){
    /*checks whether username, email, phone, and names criteria are met or not*/
    $regexUser="/^([a-z,A-Z,0-9,_\-]){1,20}$/";
    $regexEmail="/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i";
    $regexPhone="/^([0-9]{1})([0-9]{9})$/";
    $regexPassword="/^([a-z,A-Z,0-9,_\-\.\$!@#%\^&\*]){1,20}$/";
    $userValid=preg_match($regexUser,$username);
    $emailValid=preg_match($regexEmail,$email);
    // $phoneValid=preg_match($regexPhone,$phone);
    $phoneValid=true;
    $passwordValid=checkValidPhone($regexPassword,$password,$cpassword);
    $namevalid=false;
    // var_dump($passwordValid);
    if( strlen($fname)>=1 && strlen($fname)<=20 && strlen($mname)>=0 && strlen($mname)<=20 && strlen($lname)>=0 && strlen($lname<=20)){
       // echo "namecheck true";
        $namevalid=true;
        
    } 
    // echo "checking essentials";
    if($userValid==1 && $emailValid==1 && $passwordValid==true && $phoneValid){
       // echo false;
        return  $namevalid;
    }
    
    else {
       // echo "false reports";
        return false;
    }

}





?>
