<?php
include "./request.php";
include "./savenew.php";

    $data=file_get_contents('php://input');
    echo $data;
    $data2='{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"farmID" :"1_Field30", "latestDate": "20190118", "outputDate": "20190927"},"geometry":{"type":"Polygon","coordinates":[[[85.094867,25.583496],[85.094776,25.584263],[85.094891,25.584369],[85.095573,25.584425],[85.095642,25.583701],[85.095565,25.583554],[85.094867,25.583496]]]}}]}';
    $data1='{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"farmID":"26_Field23", "latestDate": "20190102","outputDate": "20190927"},"geometry":{"type":"Polygon","coordinates":[[[-115.414731,32.901813],[-115.40639,32.90184],[-115.406358,32.898498],[-115.414906,32.898435],[-115.414731,32.901813]]]}}]}';
    $data3='{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"farmID" :"1_Field15", "latestDate": "20190718", "outputDate": "20190927"},"geometry":{"type":"Polygon","coordinates":[[[-115.556538,33.154978],[-115.556538,33.161822],[-115.550938,33.161822],[-115.550938,33.154978],[-115.556538,33.154978]]]}}]}';
    $obj = json_decode($data,true);
    $inputdate=$obj['features'][0]['properties']['latestDate'];
    $outputdate= date('Ymd',strtotime('NOW'));
    $startDate = $inputdate;
    $endDate= $inputdate;
    function addDays($date,$days){
        return date('Ymd',strtotime($date)+$days*86400);

    }

    function compare($cd, $fd){
       return (int)strtotime($fd)> (int)strtotime($cd) ? true:false;
    }
    

   
    connect($obj,$outputdate,$startDate,$endDate);

    function connect ($obj,$outputdate,$startDate,$endDate) {
        if( (int)strtotime($startDate) == (int)strtotime($endDate) ){
            $startDate=addDays($startDate,0);
            $endDate= compare(addDays($startDate,7),$outputdate) ? addDays($startDate,7) : $outputdate ;
        }

        else{
            $startDate=addDays($endDate,1);
            $endDate=compare(addDays($startDate,7),$outputdate) ? addDays($startDate,7) : $outputdate;
        }

        $obj['features'][0]['properties']['latestDate']=$startDate;
        $obj['features'][0]['properties']['outputDate']=$endDate;
        $val= fetch(json_encode($obj));
        echo $val;
        try{
            if(json_decode($val,true)["status"]=="Success" ){
                if(count(json_decode($val,true)["images"])>0){
                    echo "success";
                    saveDB($val);
                    echo ($val);
                    ob_flush();
                    flush();
                }   
            }
        }
        catch(Exception $e){   
            echo ($e);
        }
        
        if(compare($endDate,$outputdate)){
            connect($obj,$outputdate,$startDate,$endDate);
        }

    }

    echo "processing complete";
    

?>