var farmInfo, farmName, farmGeo, map, reportsJSON, graphDataJSON;

$(document).ready(function(){

    farmInfo = parent.load();
 
    $.ajax({
        type: "POST",
        url: "./api/getGeoJson.php",
        data: {"field_id": farmInfo["Field_ID"]},
        dataType: "text",
        success: function(data){
            farmGeo = JSON.parse(data);
            loadOnMap(farmGeo);
        }
  });

  $.ajax({
    type: "POST",
    url: "./api/getGraphData.php",
    data: {"field_id": farmInfo["Field_ID"]},
    dataType: "text",
    success: function(data){
        graphDataJSON = JSON.parse(data);
        loadGraph(graphDataJSON);
        calculateChange(graphDataJSON);
    }
});

  $.ajax({
    type: "POST",
    url: "./api/getLatestReports.php",
    data: {"field_id": farmInfo["Field_ID"]},
    dataType: "text",
    success: function(data){
        reportsJSON = JSON.parse(data);
        console.log(reportsJSON);
        drawImageOnMap(reportsJSON[0]["imageurl"], farmGeo);
    }
});
   $("#farmName").html(farmInfo["Field_ID"]);
   

    // var hybrid = L.gridLayer.googleMutant({ type: 'hybrid' }).addTo(map);
   map = L.map('image-container').setView([51.505, -0.09], 13);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    // var hybrid = L.gridLayer.googleMutant({ type: 'hybrid' }).addTo(map);
});


$("#current").click(function(){
    drawImageOnMap(reportsJSON[0]["imageurl"], farmGeo);
});

$("#previous").click(function(){
    drawImageOnMap(reportsJSON[1]["imageurl"], farmGeo);
});

function calculateChange(graphDataJSON){
    console.log(graphDataJSON);
    var lastNDVI = parseFloat(graphDataJSON[graphDataJSON.length-1]["NDVI"]);
    var prevNDVI = parseFloat(graphDataJSON[graphDataJSON.length-2]["NDVI"]);
    var change = ((lastNDVI-prevNDVI)/prevNDVI)*100;
    console.log(change);
    console.log(lastNDVI);
    $("#detailsAvg").html(lastNDVI);
    $("#detailsChange").html( (Math.round(change)).toString() + " %");

}

function loadGraph(graphDataJSON){
    console.log(graphDataJSON);
    var labelsArray = [];
    var dataArray = [];
    for(i=0; i<graphDataJSON.length;i++){
        labelsArray.push(graphDataJSON[i]["sat_time"].split(" ")[0]);
        dataArray.push(parseFloat(graphDataJSON[i]["NDVI"]));
    }

    console.log(labelsArray);
    console.log(dataArray);
    var ctx = document.getElementById('myChart').getContext('2d');

/*** Gradient ***/
var gradient = ctx.createLinearGradient(0, 0, 0, 400);
    gradient.addColorStop(0, 'rgba(	0, 176, 116,0.8)');   
    gradient.addColorStop(1, 'rgba(	0, 176, 116,0)');
/***************/

var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: labelsArray,
        datasets: [{
            label: 'Average NDVI',
            backgroundColor: gradient,
            fillColor: gradient,
            borderColor: 'rgb(	0, 176, 116,1)',
            data: dataArray
        }]
    },

    // Configuration options go here
    options: {
        // scales: {
        //     yAxes: [{
        //         ticks: {
        //             suggestedMin: 0,
        //             suggestedMax: 1
        //         }
        //     }]
        // }
    }
});
}

function loadOnMap(geo){
    console.log(geo);
    var myLayer = L.geoJSON(geo, {style:{ color: '#06d633', fillOpacity: '0' }}).addTo(map);
    // myLayer.addData(geo);
    map.fitBounds(myLayer.getBounds());
    if(map.getZoom() > 17){
    map.setZoom(17);
    }
}


function drawImageOnMap(myUrl, polygon) {
  
    geoPoly = L.geoJson(polygon);
    bounds = geoPoly.getBounds();
    var imagelayer = L.imageOverlay(myUrl, bounds);
    removeExistingImages();
    try {
        imagelayer.addTo(map);
        console.log(imagelayer);
    } catch {
        console.log("error");
    }
}

function removeExistingImages() {
    map.eachLayer(function(layer) {
        if (layer instanceof L.ImageOverlay) {
            map.removeLayer(layer);
        }
    });
};

$("#back").click(function(){
    console.log("Back");
    window.parent.$('#dialog-details').iziModal('close');
});


