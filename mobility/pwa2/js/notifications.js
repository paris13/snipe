// fetch notifications
var alldata, fieldID, userId;
$(document).ready(function(){
    $.ajax(
        './api/fetchNotifications.php',
        {
            success: function(data) {
              console.log('AJAX call was successful!');
              console.log('Data from the server' + data);
              alldata=JSON.parse(data);
              buildDOM(alldata);
            },
            error: function() {
              alert('There was some error performing the AJAX call!');
            }
         }
      );

      $.ajax(
        './api/getUser.php',
        {
            success: function(data) {
              console.log('AJAX call was successful!');
              console.log(data);
              userId = data;
              if(userId == 0){
                  alert("Please login first");
                  window.location = "./index.html";
              }
              $("#userName").html(userId);
            },
            error: function() {
              alert('There was some error performing the AJAX call!');
            }
         }
      );

});

// -- fetch notifications

// create notifications

function buildDOM(data) {
    for(i=0; i<data.length;i++){
        var rowDiv = $("<div></div>").addClass("row notification-card");   // Create with jQuery
        var statusDiv = $("<div></div>").addClass("notification-status");
        var notificationContentDiv = $("<div></div>").addClass("row notification-content-row");
        var statisticsDiv = $("<div></div>").addClass("col-6");
        statisticsDiv.css("display","block");
        var farmdetailsDiv = $("<div></div>").addClass("col-6 farmdetails");
        var statisticsDate = $("<p></p>").addClass("mx-0 my-0");
        statisticsDate.html("<span>"+data[i]["Date"]+"</span>");
        var statisticsWeek = $("<p></p>");
        statisticsWeek.html("Crop cycle week : <span>"+ data[i]["Week"] + "</span>");
        var circlesHolderDiv = $("<div></div>").addClass("row");
        var avgCircleHolderDiv = $("<div></div>").addClass("col-6 px-0 py-0");
        var changeCircleHolderDiv = $("<div></div>").addClass("col-6 px-0 py-0 hcenter_responsive");
        var avgCircleDiv = $("<div></div>").addClass("circle circle-avg ml-0").html("<p>"+data[i]["Avg"]+"</p>");
        var avgCirclePara = $("<p></p>").html("Avg. NDVI");
        var changeCircleDiv = $("<div></div>").addClass("circle").html("<p>"+data[i]["Changed"]+" %</p>");
        var changeCirclePara = $("<p></p>").html("Weekly change");
        var farmdetailsHolderDiv = $("<div></div>");
        var farmNamePara = $("<p></p>").addClass("unread-title mx-0 my-0").html("Farm name : <span>" + data[i]["Field_ID"] + "</span>");
        var cropNamePara = $("<p></p>").addClass("mx-0 my-0").html("Crop : <span>" + data[i]["Crop"] + "</span>");
        var areaPara = $("<p></p>").addClass("mx-0 my-0").html("Area : <span>" + data[i]["Area"] + " acres</span>");
        var sowingdatePara = $("<p></p>").addClass("mx-0 my-0").html("Sowing date : <span>" + data[i]["Sowing"] + "</span>");
        var locationPara = $("<p></p>").html("Location : <span>" + data[i]["Location"] + "</span>");
        farmdetailsHolderDiv.append(farmNamePara);
        farmdetailsHolderDiv.append(cropNamePara);
        farmdetailsHolderDiv.append(areaPara);
        farmdetailsHolderDiv.append(sowingdatePara);
        farmdetailsHolderDiv.append(locationPara);
        farmdetailsDiv.append(farmdetailsHolderDiv);
        avgCircleHolderDiv.append(avgCircleDiv);
        avgCircleHolderDiv.append(avgCirclePara);
        changeCircleHolderDiv.append(changeCircleDiv);
        changeCircleHolderDiv.append(changeCirclePara);
        circlesHolderDiv.append(avgCircleHolderDiv);
        circlesHolderDiv.append(changeCircleHolderDiv);
        statisticsDiv.append(statisticsDate);
        statisticsDiv.append(statisticsWeek);
        statisticsDiv.append(circlesHolderDiv);
        notificationContentDiv.append(statisticsDiv);
        notificationContentDiv.append(farmdetailsDiv);
        rowDiv.append(statusDiv);
        rowDiv.append(notificationContentDiv);
        rowDiv.attr("name", data[i]["Field_ID"]);
        rowDiv.attr("id", data[i]["Notification_ID"]);

        // add conditional code for alert coloring
        if(parseFloat(data[i]["Changed"]) >= 0){
            rowDiv.addClass("notif");
            statusDiv.addClass("notification-status-green");
            changeCircleDiv.addClass("circle-inc");
        }

        else{
            rowDiv.addClass("alertnotif");
            statusDiv.addClass("notification-status-yellow");
            changeCircleDiv.addClass("circle-dec");
        }

        if(parseFloat(data[i]["NRead"])){
            rowDiv.removeClass('read unread');
            rowDiv.addClass('read');
        }

        else{
            rowDiv.removeClass('read unread');
            rowDiv.addClass('unread');
        }

        $("#notification-holder").append(rowDiv);
    }
}

// -- create notifications

//top buttons

$('#allread').click(function(){
    var unreadNo = $(".unread").length;
    if(unreadNo){
    $( "#dialog-confirm" ).attr("title","Mark all notifications as read");
    $("#unreadno").html(unreadNo);
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
          "Mark as read": function() {    
            var idArray = [];
            $('.unread').each(function () {
                idArray.push(this.id);
            });     
            updateStatus(idArray);   
            $('.notification-card').removeClass('unread');
            $('.notification-card').removeClass('read');
            $('.notification-card').addClass('read');
            $( this ).dialog( "close" );
          },
          Cancel: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    }

    else {
        alert("No unread notifications");
    }
    
});

$('#alertsonly').click(function(){
    if($(this).prop("checked") == true){
        $('.notif').hide()
    }
    else if($(this).prop("checked") == false){
        $('.notif').show()
    }
});

// -- top buttons


// clicking on notification row

// $("#dialog-details").dialog({
//     autoOpen: false,
//     modal: true,
//     open: function(ev, ui){
//              $('#detailsIframe').attr('src','./details.html');
//           }
// });

$("#dialog-details").iziModal({
    title: '',
    subtitle: '',
    headerColor: '#88A0B9',
    iframe: true,
    iframeHeight: 800,
    iframeURL: "./details.html",
    closeOnEscape: true,
    closeButton: true
});



$(document).on('click', '.notification-card', function(){
    // console.log($('iframe').contents().find("#back").html());
    fieldID = $(this).attr("name");
    if($(this).hasClass("unread")){
    var notifID = $(this).attr("id");
    var idList = new Array();
    idList.push(notifID);
    updateStatus(idList);
    $(this).removeClass("unread");
    $(this).addClass("read");
    }
    // loadDetails(fieldID);
    // $("#dialog-details").dialog('open');
    $('#dialog-details').iziModal('open');
        
});



function updateStatus(idList){
    $.post("./api/updateStatus.php",
  {
    arr : idList
  },
  function(data, status){
    console.log("Data: " + data + "\nStatus: " + status);
  });
  
};

function getGeoJSON(fieldID){
    $.ajax({
        type: "POST",
        url: "./api/getGeoJson.php",
        data: {"field_id": fieldID},
        dataType: "text",
        success: function(data){
            console.log(JSON.parse(data));
            return(data);
        }
  });
};


$('iframe').contents().find("#back").click(function(){
    console.log("Back");
});


// -- clicking on notification row


function load(){
    console.log(fieldID);
    var results = alldata.filter(function(obj) {
        // Return true to keep the element
        return obj.Field_ID === fieldID;
    });
    var farminfo = results[0];
    return farminfo;
};
// load details of farm


// -- load details of farm
