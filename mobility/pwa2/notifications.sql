-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 18, 2020 at 08:15 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meow`
--

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `Notification_ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `Field_ID` varchar(11) NOT NULL,
  `Crop` varchar(11) NOT NULL,
  `Area` float NOT NULL,
  `Sowing` varchar(50) NOT NULL,
  `Location` varchar(50) NOT NULL,
  `Date` date NOT NULL,
  `Week` int(11) NOT NULL,
  `Avg` float NOT NULL,
  `Changed` float NOT NULL,
  `NRead` tinyint(1) NOT NULL,
  PRIMARY KEY (`Notification_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`Notification_ID`, `user_id`, `Field_ID`, `Crop`, `Area`, `Sowing`, `Location`, `Date`, `Week`, `Avg`, `Changed`, `NRead`) VALUES
(1, 13, '1_Field1', 'Grapes', 22, '23 June 2019', 'Nashik, Maharashtra', '2020-04-01', 23, 0.34, 0.1, 0),
(2, 13, '1_Field2', 'Grapes', 22, '23 June 2019', 'Nashik, Maharashtra', '2020-03-18', 23, 0.34, 0.1, 1),
(3, 6, '1_Field3', 'Grapes', 21.2, '23 July 2019', 'Sangli, Maharashtra', '2020-03-29', 21, 0.32, -0.1, 0),
(4, 13, 'Field_1', 'Grapes', 24, '23 Jan 2019', '', '2020-03-12', 12, 0.45, -0.02, 0),
(5, 13, 'Field_3', 'Grapes', 24, '23 Jan 2019', '', '2020-03-22', 12, 0.45, 0.02, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
