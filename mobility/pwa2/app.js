function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }


navigatorEligibility  = new Promise(function(resolve,reject){
    if(!'serviceworker' in navigator){
        console.log("doesn't have service worker support");
        reject("will not work");
    }

    else{
        resolve("workingfine");

    }

});

navigatorEligibility
    .then((string)=>{
        console.log(string);
        requestPermission
        .then((result)=>{
            // alert(result);
            if( result == "granted"){
            registerServiceWorker();
        }
        },(error)=>{alert(error)});
    }
    ,(string)=>{
        console.log("not working at any cost")
    });

requestPermission = new Promise((resolve,reject)=>{

    const askPermission = Notification.requestPermission();
    askPermission.then((result)=>{resolve(result);},(error)=>{reject(error);});

});


function registerServiceWorker(){
    // alert("registering service worker");
    navigator.serviceWorker.register('serviceworker.js')
    .then((registration)=>{console.log("registration complete"+registration);
        const subscribeOptions = {
          userVisibleOnly: true,
          applicationServerKey: urlBase64ToUint8Array(
            "BJAKVHFlMhlklQn742C4BRxmAxvil-2UnL6mroEWC53R_kAplqHNtfq4nDHZSkgQtl1D6906cGP1CSKRvY8NVgQ"
          )
        };
        return registration.pushManager.subscribe(subscribeOptions);
        }
        ,(error)=>{"some error occured "+error;}
    ).then((pushSubscription)=>{
        console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
        return pushSubscription;
    });
}

function subscribeUserToPush() {
    return navigator.serviceWorker.register('/service-worker.js')
    .then(function(registration) {
      const subscribeOptions = {
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(
          'BEl62iUYgUivxIkv69yViEuiBIa-Ib9-SkvMeAtA3LFgDzkrxZJjSgSnfckjBJuBkr3qBUYIHBQFLXYp5Nksh8U'
        )
      };
  
      return registration.pushManager.subscribe(subscribeOptions);
    })
    .then(function(pushSubscription) {
      console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
      return pushSubscription;
    });
  }

