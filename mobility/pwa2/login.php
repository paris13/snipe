<?php
/* this page checks whether the login credentials are true. if true this page redirects to index.php */

session_start();
require "./sqlconfig.php";
if (!isset($_SERVER['HTTP_REFERER'])) {
  echo 'Unauthorized Access';
  // echo "<script>window.location='./registration.php'</script>";
  exit();
}
if ($_SERVER["REQUEST_METHOD"] == 'POST') {
  $name = $_POST['username'];
  $password = $_POST['pass'];
  $result = $con->prepare("select user_name, user_id from user_credentials where user_id in (select user_id from user_info where email= ?) and password = ?");
  $result->bind_param('ss', $name, $password);
  $result->execute();
  $result->store_result();
  $result->bind_result($username, $user_id);
  $result->fetch();
  // echo($username);
  $add = $result->num_rows;
  // echo var_dump($add);
  $con->stat();
  if ($add == 1) {
      $_SESSION['name'] = $username;
      $_SESSION['userid'] = $user_id;
      // echo "<script>alert('Logged in');</script>";
      echo "<script>window.location='./notifications.html'</script>";
      
    
  } else {
    echo "<script>
      alert('Wrong Email or Password');
      </script>";
    echo "<script>window.location='./index.html'</script>";
  }
}
//  session save path------ copy it in php ini editor when problem is solved /var/cpanel/php/sessions/ea-php56
?>