var cacheName = 'PWA';
var filesToCache = [
    '/',    
    'index.html',
    'notifications.html',
    'details.html',    
    'css/details.css',
    'css/form.css',  
    'css/main.css',  
    'css/materialize.css',  
    'css/materialize.min.css',  
    'css/notifications.css',  
    'css/responsive.css',  
    'css/util.css',
    'js/details.js', 
    'js/form.js',
    'js/materialize.js',
    'js/materialize.min.js',
    'js/notifications.js',
    'js/main.js'  ]; 
self.addEventListener('install', function(e) { 
e.waitUntil(
caches.open(cacheName).then(function(cache) { 
return cache.addAll(filesToCache);   
})    
);  
}); 
/* Serve cached content when offline */ 
self.addEventListener('fetch', function(e) {  
e.respondWith(      caches.match(e.request).then(function(response) {  
return response || fetch(e.request);
})   
);  
});