<?php
if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access';
    exit();
}
class Fields{

    var $fieldid;
    var $crop;
    var $geometry;
    var $sowingDate;
    var $area;
    var $dateCreate;
    var $longitude;
    var $latitude;
    var $location;
    var $farmName;


    function setfieldid($fieldid){
        $this->fieldid=$fieldid;
    }

    function setCrop($crop){
        $this->crop=$crop;
    }

    function setGeometry($geometry){
        $this->geometry=$geometry;
    }

    function setsowingDate($sowingDate){
        $this->sowingDate=$sowingDate;
    }

    function setArea($area){
        $this->area=$area;
    }

    function setLatitude($latitude){
        $this->latitude=$latitude;
    }

    function setLongitude($longitude){
        $this->longitude=$longitude;
    }

    function setCreatedDate($dateCreate){
        $this->dateCreate=$dateCreate;
    }
    function setLocation($location){
        $this->location=$location;
    }
    function setfarmName($farmName){
        $this->farmName=$farmName;
    }
}
/* fetches existing fields from field table for given user */
session_start();
include "./sqlconfig.php";
    $fetcher=
    "select field_id, crop, geo, sowing_date, acreage, latitude, longitude, dateCreated, location,farm_name from fields where user_id in (select user_id from user_credentials where user_name =?)";
    $stmt=$con->prepare($fetcher);
    $stmt->bind_param("s", $_SESSION['name']);
    $stmt->execute();
    $stmt->bind_result($temp,$temp1,$temp2, $temp3, $temp4, $temp5, $temp6, $temp7,$temp8,$temp9);
    $resultset=array();
    $i=0;
    while($stmt->fetch())
    {
        $fd=new Fields();
        $fd->setfieldid($temp);
        $fd->setCrop($temp1);
        $fd->setGeometry($temp2);
        $fd-> setsowingDate($temp3);
        $fd-> setArea($temp4);
        $fd-> setLatitude($temp5);
        $fd-> setLongitude($temp6);
        $fd-> setCreatedDate($temp7);
        $fd->setLocation($temp8);
        $fd->setfarmName($temp9);

        $resultset[$i]=$fd;
        $i++;
    }
    
    echo json_encode($resultset);
    
  ?>



