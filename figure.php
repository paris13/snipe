<?php

if (!isset($_SERVER['HTTP_REFERER'])) {
   echo 'Unauthorized Access';
   exit();
}
/* fetches barchart data for the given field's certain date from table field_image*/
class FigureData
{

   var $myData;
   var $myUrl;

   function setMyUrl($NDVI,$NDWI,$TRUE)
   {
      $this->myUrl['NDVI'] = $NDVI;
      $this->myUrl['NDWI'] = $NDWI;
      $this->myUrl['TRUE'] = $TRUE;
   }

   function setMyData($data)
   {
      $this->myData = $data;
   }
   function getMyUrl()
   {
      return $this->myUrl;
   }
   function getMyData()
   {
      return $this->myData;
   }
}
session_start();
include "./sqlconfig.php";
$field_id = $_REQUEST['fieldName'];
$field_date = $_REQUEST['fieldDate'];
$fetcher =
   "select gndvi,yndvi,rndvi,imageurl,ndwi_path, true_path from field_image where field_id=? and date(sat_time)=?";

$stmt = $con->prepare($fetcher);
$stmt->bind_param("ss", $field_id, $field_date);
$stmt->execute();
$stmt->bind_result($temp, $temp2, $temp3, $temp4,$ndwiPath,$truePath);

$resultset = array();
$stmt->fetch();
$resultset[0] = $temp;
$resultset[1] = $temp2;
$resultset[2] = $temp3;
// $resultset[3] = $temp4;
$obj = new FigureData;
$obj->setMyUrl($temp4,$ndwiPath,$truePath);
$obj->setMyData($resultset);

//    $i=0;

//    while($stmt->fetch())
//    {   

//        $temp->gndvi;

//        $resultset[$i]=$obj;
//        $i++;
//    }

echo json_encode($obj);
