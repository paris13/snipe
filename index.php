<?php
// if(!isset($_SERVER['HTTP_REFERER']))
// {     echo 'Unauthorized Access'; 
//     exit();
// }
session_start();
// echo(phpinfo());
include "sqlconfig.php";

if (!isset($_SESSION['name'])) {
    echo "<script>window.location='registration.php'</script>";
} else {
    $pr = $_SESSION['name'];
    // echo "<script>alert('$pr')</script>";
}

include "checkpayment.php";


// if ($_SESSION['payment_status'] != "complete") {
//     echo "<script>window.location='payment.php'</script>";
// } else {
//     // echo "<script>alert('$pr')</script>";
// }


?>
<html>
        <style>
                .data-circle {
                    fill: #fff;
                    stroke:#e62027;
                    stroke-width: 2px;
                }

            
                body {
                    overflow: hidden !important;
                    background-size: cover;
                }
            
            </style>
    <head>
        <title>
            AgriForetell
        </title>
        <link rel="stylesheet" href="./assets/css/random.css" />
        <link rel="shortcut icon" type="image/x-icon" href="./assets/images/aflogo.ico">
        <link rel="stylesheet" href="./assets/css/timeline.css" />
        <link rel="stylesheet" href="./assets/css/index.css">
        <link rel="stylesheet" href="./assets/css/responsive.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
        <link rel="stylesheet" href="./assets/css/leaflet-search.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.css" />
        <link rel="stylesheet" href="./assets/fontawesome/css/all.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
        <link rel="stylesheet" href="./assets/css/MarkerCluster.css" />
        <link rel="stylesheet" href="./assets/css/MarkerCluster.Default.css" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKCXGV0LF4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-TKCXGV0LF4');
        </script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3"></script>
        <script src="https://cdn.jsdelivr.net/npm/hammerjs@2.0.8"></script>
        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.7"></script>
    </head>
    <body>
        <div class="row" id="fullDiv">
           
            <!-- Content div starts -->
            <div class="col-6 zero-padding" id="contentDiv">
                <div class="row zero-margin">
                 <!-- Branding starts -->
                <div class="col-12" id="branding" style="background:white">
                    <img src="https://www.agriforetell.com/assets/img/aflogo2.png" height="50em">
                    <span style="float:right; color:white; padding:10px">
                    <a id="accountDropdown" class="pointed"> 
                                <div class="dropdown">
                                    <a class="pointed" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#888">
                                    Welcome <?php echo ($_SESSION['name']) ?> <i class="fa fa-caret-square-down"></i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="accountDropdown">
                                      <button class="dropdown-item" id="userAccount"><i class="fa fa-user"></i> My account</button>
                                      <div id="tutorialNav" aria-labelledby="accountDropdown" class="dropdown-item" style="cursor:pointer">
                            <i class="fa fa-question-circle"></i> Watch tutorial
                    </div>
                    <div class="dropdown-item"  aria-labelledby="accountDropdown">
                        <a href="https://agriforetell.com/#contact" style="color: black;" target="new" ><i class="fas fa-phone-alt"></i> Contact AgriForetell</a>
                    </div>
                    <div class="dropdown-item" aria-labelledby="accountDropdown" >
                        <a style="color: black;" href="./loggedout.php"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </div>
                                    </div>
                    
                    </div></a>
                    </span>
                </div>
                <!-- Branding ends -->
                
                <!-- Vertical navigation starts -->
                <!-- <div class="col-1 zero-padding height93"  id="verticalNav">
                <div class="zero-padding">
                    <div id="newFieldNav" class="navlink showEmptyField" data-toggle="tooltip" data-placement="right" data-html="true" title="Add new field">
                            <i class="fa fa-plus"></i>
                    </div>
                    <hr>
                    <div id="addedFieldsNav" class="navlink showAddedFields" data-toggle="tooltip" data-placement="right" data-html="true" title="My added fields">    
                        <i class="fa fa-leaf"></i>
                    </div>
                    <hr>
                    <div id="tutorialNav" class="navlink" data-toggle="tooltip" data-placement="right" data-html="true" title="Watch tutorial">
                            <i class="fa fa-question-circle"></i>
                    </div>
                    <hr>
                    <div class="navlink " data-toggle="tooltip" data-placement="right" data-html="true" title="Contact AgriForetell">
                        <a href="https://ignisnova.com/#contact" style="color: black;" target="new" ><i class="fas fa-phone-alt"></i></a>
                    </div>
                    <hr>
                    <div class="navlink" data-toggle="tooltip" data-placement="right" data-html="true" title="Logout">
                        <a style="color: black;" href="./loggedout.php"><i class="fas fa-sign-out-alt"></i></a>
                    </div>
                    <hr>
                </div>
                </div> -->
                <!-- Vertical navigation ends -->

                <!-- Remaining content starts -->  
                <div class="col-4" style="padding:0; text-align:center; min-height:92vh">
                <span id="noaddedmessage" style="display:none; text-align:center; color:#ababab; font-style:italic">No fields added yet.</span>
                <!-- Field list starts -->
                <div class="zero-margin " id = "fieldList" style="display:none;">
                        <div class="row" id="farmMenuRow">
                        <div class="col-8 zero-margin">
                        <!-- <h6 class="instruction m5 " id="fieldStatus">Added fields</h6>  -->
                        <button id="newFieldNav" class="showEmptyField btn btn-common" style="margin:5px">
                            <i class="fa fa-plus"></i>Add new field
                    </button>
                        </div>
                        <div class="col-1 m5 zero-margin zero-padding">
                            <span id="sort" class="menubtn">
                            <div class="dropdown">
                                    <a id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="fa fa-sort-amount-down-alt"></i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <button class="dropdown-item" onclick="displayFields(5)" >Sowing date - latest first</button>
                                      <button class="dropdown-item" onclick="displayFields(4)" >Sowing date - oldest first</button>
                                      <button class="dropdown-item" onclick="displayFields(1)" >Created date -latest first</button>
                                      <button class="dropdown-item" onclick="displayFields(0)" >Created date - oldest first</button>
                                      <button class="dropdown-item" onclick="displayFields(3)" >Area - highest to lowest</button>
                                      <button class="dropdown-item" onclick="displayFields(2)" >Area - lowest to highest</button>
                                    </div>
                                  </div>
                                  </span>
                        </div>
                        <div class="col-1 m5 zero-margin zero-padding">
                        <span class="menubtn">
                                    <a id="exportAllData" data-toggle="tooltip" data-placement="right" data-html="true" title="Download all data to Excel">
                                      <i class="fa fa-download"></i>
                                    </a>
                        </span>
                        </div>
                        </div>

                        <!-- <button id="exportAllData" class="btn btn-common-nogradient ml10"><i class="fa fa-download"></i> Export all data to Excel</button> -->

                        <div id="fieldpopulate" class="list-group">
                                                
                            </div>

                              
                    

                </div>   <!-- Field list ends -->
                </div>
                <!-- Add new field empty starts -->
                <div class="col-8 hcenter" style="display:none; padding-top:15vh;" id = "addFieldEmpty">
                        <img src="./assets/images/field.svg" style="height: 10em; width: 10em"> <br><br><br>
                        <span class="instruction">Click the button to add a new field. </span> <br><br>
                        <button id="createField" class="btn btn-common"><i class="fa fa-plus"></i> New field</button>
                     
                </div><!-- Add new field empty ends -->

                <!-- Add new field form starts -->
                <div class="col-8 p10 vcenter" style="display:none; padding-top:5vh! important" id = "addFieldForm">
                    
                        <div id="newFieldRegistration">
                                
                                <div class="centertext">  
                                        <h4>Add farm details</h4>  
                                        <img src="./assets/images/farm.svg" style="height: 10em; width: 10em"><br> <br>
                                    </div>
                                <!-- <div class="form-group " style="display: none">
                                    <label for="location">Farm location </label>
                                    <input id="location" class="" type="text" placeholder="(District or state)">
                                </div> commented out for location id conflict with the new text area on jan 12 2020 --> 
                                 
                                <label for="farmName">Farm-name</label>
                                <input type="text"  id ="farmName" size="50" class="form-control" />
                                <label for="crop">Crop</label>
                                <input type="text"  id ="cropInput" list="crop"  size="55" class="form-control" />
                                <!-- class="custom-select" -->
                                <datalist  id="crop">
                                    <option value="">No value</option>
                                </datalist>
                                <!-- added text area for filling other information -->
                                <label for ="location">Description (optional)</label>
                                <textarea name="nonamelocation" placeholder="optional" id="location" cols="58" rows="4" class="form-control"></textarea>
            
                                <div class="form-group "><label for="sowingdate">Sowing Date</label>
                                    <input class="form-control" type="date" id="sowingdate" placeholder="sowingdate">
                                </div>

                                
                                <div id="mapInstruction">
                                    <p>Click on the 'Draw Polygon' button (<img class="inlineImage" src = "./assets/images/polygon_icon.png"> or <img class="inlineImage" src = "./assets/images/rectangle_icon.png">) on the map, then draw a polygon around the farm boundary.</p>
                                </div>
                                <input type="radio" disabled id="polygonradio" name="polygonradio" value="other"><span id="polyspan" class="notDrawn" style="color: var(--danger-color  );"> Farm not selected on map </span>
                                <br>
                                <div><span id="submitstatus"></span></div>
                                <br>
                                <div class="button">
                                    <button id="saveField" class="btn btn-common"> <i class="fa fa-save"></i> Save field
                                    </button>
                                    <button id="cancelSaveField" class="btn btn-danger-hollow showEmptyField"> <i class="fa fa-times"></i> Cancel
                                    </button>
                                </div>
                            </div>
                </div><!-- Add new field form ends -->


                <!-- edit field form starts -->
                <div class="col-8 p10 vcenter" style="display:none; padding-top:5vh! important" id = "editFieldForm">
                    
                        <div id="newFieldRegistration">
                                
                                <div class="centertext">  
                                        <h4>Edit farm details</h4>  
                                        <img src="./assets/images/farm.svg" style="height: 10em; width: 10em"><br> <br>
                                    </div>
                                <!-- <div class="form-group " style="display: none">
                                    <label for="location">Farm location </label>
                                    <input id="location" class="" type="text" placeholder="(District or state)">
                                </div> commented out for location id conflict with the new text area on jan 12 2020 --> 
                                 
                
                                <label for="editCrop">Crop</label>
                                <input type="text"  id ="editCropInput" list="editCrop"  size="55"  />
                                <!-- class="custom-select" -->
                                <datalist  id="editCrop">
                                    <option value="">No value</option>
                                </datalist>
                                <!-- added text area for filling other information -->
                                <label for ="editLocation">Description (optional)</label>
                                <textarea  placeholder="optional" id="editLocation" cols="58" rows="4"></textarea>
            
                                <div class="form-group "><label for="editSowingdate">Sowing Date</label>
                                    <input class="form-control" type="date" id="editSowingdate" placeholder="sowingdate">
                                </div>

                                <div><span id="editSubmitStatus"></span></div>

                                <div class="button">
                                    <button id="saveEdit" class="btn btn-common"> <i class="fa fa-save"></i> Save Changes
                                    </button>
                                    <button id="cancelEditField" class="btn btn-danger-hollow"> <i class="fa fa-times"></i> Cancel Edit 
                                    </button>
                                </div>
                            </div>
                </div><!-- edit field form ends -->

                

                <!-- Field details starts -->
                <div class="col-8 zero-margin " style="display:none;" id = "fieldDetails">
                    <div class="row" id=fdrow>
                        <div class="col-2 vhcenter zero-padding " style="display:none" id="backbuttondiv">
                        <a id="backToFields" class="loadFields pointed pt-5">
                            <i class="fas fa-long-arrow-alt-left"></i>  
                          </a>
                        </div>
                        <div class="col-5 vhcenter " id="fieldnamediv">
                            <span class="instruction m5 p10" id="fieldName">User_Field1</span> 
                        </div>
                        <div class="col-5 vhcenter" id="optionsbuttondiv">
                            <a id="exportField" class="pointed underlined pt-5"> 
                                <div class="dropdown">
                                    <a class="pointed" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Options <i class="fas fa-ellipsis-v"></i>
                                      <!-- <i class="fa fa-caret-square-down"></i> -->
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <!--<button class="dropdown-item" id="editFarm">Edit farm</button>-->
                                        <button class="dropdown-item" id="exportFarm">Export to Excel</button>
                                        <button class="dropdown-item" id="deleteField" >Delete Field</button>
                                    </div>
                                  </div></a>
                        </div>
                        <!-- dialog boxes -->


                        <div id="dialog-confirm" title="Delete this field?">
                            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>This field will be permanently deleted and cannot be recovered. Are you sure?</p>
                        </div>
                        <div id="dialog-confirm-image" title="Delete data from this field?">
                            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>This data will be permanently deleted from field and cannot be recovered. Are you sure?</p>
                        </div>
                        


                        <!-- diaglog boxes end -->
                        <div class="col-12 mt5 mb5" id="hrSeparator">
                            <hr>
                        </div>
                        <div class="col-12">
                               <div class="row p5">
                                   <div class="col-12  p10" id="weather">
                                    <div class="row p5" id="">
                                        <div class="col-12">
                                        <h6 class="instruction"> Current weather at farm <small> <span id="weatherTime"></span></small> </h6>
                                    </div>
                                    <div class="col-6 vhcenter" id="weatherdataLeft">
                                        <p class="weatherbig"><img src="" style="height: 40px; width: 40px"  alt="" id="iconWeather"> <span id="descWeather"></span></p>
                                    </div>
                                    <div class="col-6 vhcenter" id="weatherdataRight">
                                            <span class="weathericon"><i class="fas instruction  fa-tint"></i> <span class="weatherdata" id="humidity"> 67% </span> <small> humidity </small></span>
                                            <hr>
                                            <span class="weathericon"><i class="fas instruction  fa-cloud"></i> <span class="weatherdata" id="cloudCover"> </span><small> cloud cover </small></span>
                                            <hr>
                                            <span class="weathericon"><i class="fas instruction  fa-thermometer-three-quarters"></i> <span class="weatherdata" id="temperature">33 Celsius</span></span>
                                        </div>
                                    </div>
                                   </div>
                               </div>
                        </div>
                        <div class="col-12 vhcenter" id="calculatedData">
                            <div class="row">
                            <!--<div class="col-12" id='dropdownDiv'> </div>-->
                            <!-- FIXME to change NDVI title in JS -->
                            <div class="col-12" id="graphDiv">
                                    <div class="row" id='dropdownDiv'> </div>   
                                    <p class="col-12 pt10" id="ndviTitle"></p>
                                    <div class="col-12">
                                        <button id="dwonloadBtn" class="btn btn-common-nogradient btn-sm" onclick="downloadShape()">Download Shapefile</button>
                                    </div>
                                    <!-- <div id="line"></div> -->
                                    <!-- DONE uncomment for chartjs  -->
                                    <canvas id="line"></canvas>
                            </div>
                            
                        </div>
                        </div>
                        
                            <div class="col-12 vhcenter">
                                    <div class="row " style="padding:5%">
                                    <div class="col-6 " id="fetchButtonDiv">
                                            <button class="btn btn-common-nogradient" id="fetch"><i class="fa fa-download"></i> Fetch latest data</button>
                                    </div>
                                    <div class="col-6">
                                        <button id="deletesatbtn" class="btn btn-danger" onclick="deletefieldcreate()">Delete Data</button>
                                    </div>
                                </div>
                                </div>

                                <div id="whateverdiv"></div>
                        
                    </div>
            
                </div><!-- Field details ends -->

                <!-- Remaining content ends -->
            </div>
            </div>
            <!-- Content div ends -->
            <div class="col-6" id="mapDiv">
                
                <div id="map"></div>
                <div class="col-12 vhcenter" id="boxTimeline">
                        <div class="row p5">
                        <div class="col-12 p10" id="timelineDiv">
                                <section class="cd-horizontal-timeline">
                                <div id="timeline">
                                    <div class="events-wrapper">
                                        <div class="events">
                                            <!-- add this style="list-style: none;" -->
                                            <ol style="list-style: none;" id="akashList">
                                            </ol>
                                            <script src=list.js></script>
                                            <span class="filling-line" aria-hidden="true"></span>
                                        </div> <!-- .events -->
                                    </div> <!-- .events-wrapper -->
        
                                    <ul class="cd-timeline-navigation" style="list-style: none;">
                                        <li><a href="#0" class="prev inactive">Prev</a></li>
                                        <li><a href="#0" class="next">Next</a></li>
                                    </ul> <!-- .cd-timeline-navigation -->
                                </div>
                                </section>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
        <!-- Go to Top Link -->
    <!-- <a href="#" class="back-to-top">
    	<i class="lni-arrow-up"></i>
    </a> -->

    <div id="processLoader" class="processLoader">
        <h4 id="modal1"></h4>
        <h6 id="modal2"></h6>
            <div id="spinner" class="loader" style="display:none"></div>
        <!-- <a id="cp" href="#" rel="modal:close">Cancel processing</a> -->
        <style>
            .loader {
                border: 10px solid #e3e3e3;
                /* Light grey */
                border-top: 10px solid rgb(0, 197, 66);
                /* Blue */
                border-radius: 50%;
                width: 40px;
                height: 40px;
                animation: spin 1s linear infinite;
            }

            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }

                100% {
                    transform: rotate(360deg);
                }
            }
        </style>
    </div>
    <div id="dialog-tutorial" class="vhcenter" title="Tutorial for AgriForetell">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/BwSuxh7KnX8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div id="dialog-account" title="Account details">
        <div class="row" class="vhcenter p10" id="accountRow">
        <div class="col-12 p5 vhcenter">Hello, <?php echo ($_SESSION['name']) ?><span class="accDetail" id="firstName"></span> <span class="accDetail" id="lastName"></span></div>
        <div style="display:none" class="col-12 p5 vhcenter">E-mail address :  <span class="accDetail" id="email"></span></div>
        
        <div class="col-12"><details open>
        <summary class="p10 instruction">Payment history</summary>
        <div class="p10 col-12">
        <span>Your last payment of<b> <?php echo $_SESSION["payment_amount"] ?></b> for 1 year access was on <b> <?php echo $_SESSION["payment_date"] ?></b> (YYYY-MM-DD). <br> In case of questions,  <a class="primarylink" target="new" href="https://www.agriforetell.com#contact" >please contact us here.</a></span>
        </div>
        </details></div>

        <div style="display:none" class="col-12"><details>
        <summary class="p10 instruction">Change password</summary>
        <div class="control-group p10 col-12">
            <label for="current_password" class="control-label"><small>Current Password</small></label>
            <div class="controls">
                <input type="password" name="current_password">
            </div>
        </div>
        <div class="control-group p10 col-6">
            <label for="new_password" class="control-label"><small>New Password</small></label>
            <div class="controls">
                <input type="password" name="new_password">
            </div>
        </div>
        <div class="control-group p10 col-6">
            <label for="confirm_password" class="control-label"><small>Confirm Password</small></label>
            <div class="controls">
                <input type="password" name="confirm_password">
            </div>
        </div>   
        <div class="col-4">
        <button class="btn btn-warning" id="ChangepasswordButton">Change password</button>
        </div>  
        </details></div>
        </div>
    </div>

      

    <!-- Preloader -->
    <!-- <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div> -->
    <!-- End Preloader -->
    </body>
    <link rel="stylesheet" href="./assets/css/fab.css" />
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="horizontal.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjMn0KhAXGmfNbDx2ipUO7CRkkVGyXdQ8" async defer></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src='./assets/js/Leaflet.GoogleMutant.js'></script>
    <script src="./assets/js/leaflet-search.js"></script>
    <script src="./assets/js/leaflet.markercluster-src.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://d3js.org/d3.v4.min.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="./assets/js/lineChart.js"></script>
    <script src="./graphdata.js"></script>
    <script src="./changes.js"></script>
    
    <script src="./d3.js" type="text/javascript"></script>
    <script src="./d3/d3.min.js" type="text/javascript"></script>
</html>
