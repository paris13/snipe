       
            function cc(){

            var width = d3.select("s1");
            var height = 300;
            var svg = d3.select("#d3uno")
               .append("svg")
               .attr("width", width)
               .attr("height", height);

            var group = svg.append("g")
               .attr("transform", "translate(40, 40) rotate(0)");
            
            var rect = group.append("rect")
               .attr("x", 20)
               .attr("y", 20)
               .attr("width", 60)
               .attr("height", 30)
               .attr("fill", 'yellowgreen');

            console.log(typeof(rect));
            var circle = group
               .append("circle")
               .attr("cx", 0)
               .attr("cy", 0)
               .attr("r", 30)
               .attr("fill", 'lightgreen');
            }
                
       
             data= [19, 24, 19, 14, 16, 19, 15, 14, 10, 12, 12, 16]; 
             function yy(){
                 var margin = {top: 20, right: 30, bottom: 30, left: 40},
                 width =  data.length*40,
                 height = 600 - margin.top - margin.bottom;
                var sv = d3.select("#d3uno").append('svg')
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                
                // sv = d3.select('#d3uno').append('svg').attr('height',500).attr('width',data.length*40+40);
                
                bar=sv.selectAll("g")
                .data(data)
                .enter()
                .append("g")
                .attr("transform", function(d,i){ 
                    return "translate("+ i*40+","+ (500-d*20)  +")";
                });
               
               var rect= bar.append('rect').classed('barcolumn',true).attr('width', 25).attr('height', function(d){
                    return d*20;
                }).attr("fill","#ff72ad");
               
                bar.append('text')
                .attr('x', function(d,i){
                    return i*40;
                })
                .attr('y',100)
                .attr("dx", ".1em")
                 .text(function(d) { return d; });

                 var scale = d3.scaleLinear()
                  .domain([0, d3.max(data)])
                  .range([0, height-70 ]);

                // Add scales to axis
                var y_axis = d3.axisBottom()
                .scale(scale);

                //Append group and insert axis
                sv.append("g")
                .call(y_axis).attr('transform',' translate(-10,500) rotate(270)');
                
                var x = d3.scaleBand()
                .domain(["A", "B", "C", "D", "E", "F",'G','H','I','J','K', 'L',])
                .rangeRound([0, width]);
                
                x_axis=d3.axisBottom().scale(x);
                var classer=sv.append("g");
                classer.classed('newclass',true);
                classer.call(x_axis).attr('transform',' translate(-10,500) ');

         
             }


             datum=new Array(4,6,8,6,5);
            
             function change(){
                for( i=0;i<datum.length;i++){
                    j=i+1; 
                    b=j.toString(10);
                     datum[i]=parseInt(document.getElementById(b).value);
                 }
                d3.selectAll(".bars").data(datum).attr('transform',function(d,i){
                    return "translate("+i*40+","+(500-d*5)+")";
                })
                d3.selectAll(".barcolumn").data(datum).attr('height',function(d,i){return d*5;});
                
             }


             
             function grapher(){
                 for( i=0;i<datum.length;i++){
                    j=i+1; 
                    b=j.toString(10);
                     datum[i]=parseInt(document.getElementById(b).value);
                 }
                var margin = {top: 20, right: 30, bottom: 30, left: 40},
                width =  datum.length*40,
                height = 600 - margin.top - margin.bottom;
               var sv = d3.select("#graph").append('svg')
               .attr("width", width + margin.left + margin.right)
               .attr("height", height + margin.top + margin.bottom)
               .append("g")
               .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
               
               // sv = d3.select('#d3uno').append('svg').attr('height',500).attr('width',data.length*40+40);
               
               bar=sv.selectAll("g")
               .data(datum)
               .enter()
               .append("g").classed('bars',true)
               .attr("transform", function(d,i){ 
                   return "translate("+ i*40+","+ (500-d*5)  +")";
               });
              
              var rect= bar.append('rect').classed('barcolumn',true).attr('width', 25).attr('height', function(d){
                   return 5*d;
               }).attr("fill","#ff72ad");
              
               
                var scale = d3.scaleLinear()
                 .domain([0, 100])
                 .range([0,  500]);

               // Add scales to axis
               var y_axis = d3.axisBottom()
               .scale(scale);

               //Append group and insert axis
               sv.append("g")
               .call(y_axis).attr('transform',' translate(-10,500) rotate(270)');
               
               var x = d3.scaleBand()
               .domain(["A", "B", "C", "D", "E"])
               .rangeRound([0, width]);
               
               x_axis=d3.axisBottom().scale(x);
               var classer=sv.append("g");
               classer.classed('newclass',true);
               classer.call(x_axis).attr('transform',' translate(-10,500) ');

             }

     function xx(){
            var width = 300 
            scaleFactor = 20, 
            barHeight = 30;
         
         var graph = d3.select("#d3uno")
            .append("svg")
            .attr("width", width)
            .attr("height", barHeight * data.length);
         
         var bar = graph.selectAll("g")
            .data(data)
            .enter()
            .append("g")
            .attr("transform", function(d, i) {
               return "translate(0," + i * barHeight + ")";
            });

            bar.append("rect").attr("width", function(d) {
            return d * scaleFactor;
            })
            .attr("height", barHeight - 4).attr('fill','lightgreen');
         
            bar.append("text")
            .attr("x", function(d) { return (d*scaleFactor); })
            .attr("y", barHeight / 2)
            .attr("dy", ".35em")
            .text(function(d) { return d; });
    }




             function one(){
                 console.log("button clicked");
                var xhttp = new XMLHttpRequest();
                console.log('var created');
                xhttp.onreadystatechange = function() {
                    console.log("state is changing");
                    if (true) {
                          
                        document.getElementById("s1").innerHTML = this.statusText;
                    }   
                 };
                xhttp.open("GET", "http://www.ignisnova.com/", true);
                xhttp.send();

            }

            function graph(){
                window.alert('graphing on');
                var trace1 = {
                x: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                y: [20, 14, 25, 16, 18, 22, 19, 15, 12, 16, 14, 17],
                type: 'bar',
                name: 'Primary Product',
                marker: {
                    color: 'rgb(49,130,189)',
                    opacity: 0.7,
                }
                };

                var trace2 = {
                x: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                y: [19, 14, 22, 14, 16, 19, 15, 14, 10, 12, 12, 16],
                type: 'bar',
                name: 'Secondary Product',
                marker: {
                    color: '#aa00ff',
                    opacity: 0.5
                }
                };

                var data = [trace1, trace2];

                var layout = {
                title: '2013 Sales Report',
                xaxis: {
                    tickangle: -45
                },
                barmode: 'group'
                };

Plotly.newPlot('s2', data, layout);
            }
    