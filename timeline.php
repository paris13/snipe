<?php

if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}
    /* selects all the ndvi data from sowing date of the chosen field */ 
    class Ndvi{     
        var $myData;
        var $myDate;
        var $myNdwi;

        function setMyDate($date){
        $this->myDate=$date;
        }

        function setMyData($data){
        $this->myData=$data;
        }

        function setMyNdwi($ndwi){
            $this->myNdwi=$ndwi;
        }
    }
    session_start();
include "./sqlconfig.php";
   
   $field_id=$_REQUEST['fieldid'];
   
   $fetcher=
   "select ndvi,sat_time,ndwi from field_image where field_id=? order by sat_time asc";
   
   $stmt=$con->prepare($fetcher);
   $stmt->bind_param("s", $field_id);
   $stmt->execute();
   $stmt->bind_result($temp,$temp2,$temp3);
   
   $data=array();
   $date=array();
   $ndwi=array();
   $i=0;

   while($stmt->fetch()){
    $data[$i]=$temp;
    $date[$i]=$temp2;
    $ndwi[$i]=$temp3;
    $i++;
   }



   $obj= new Ndvi;
   $obj->setMyData($data);
   $obj->setMyDate($date);
   $obj->setMyNdwi($ndwi);

    echo json_encode($obj) ;
