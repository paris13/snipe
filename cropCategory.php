<?php
/* returns all types of crops present in crops table*/
if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}
session_start();
include "./sqlconfig.php";

    // $query="create table crops (crop  varchar(30),primary key(crop))";
    $fetcher="select crop from crops;";
    $stmt=$con->query($fetcher);
    // $stmt->execute();
    // $stmt->store_result();
    $resultset=array();
    $i=0; 
    while($temp=$stmt->fetch_Object())
    {
        $resultset[$i]=$temp->crop;
        $i++;
    }
    echo json_encode($resultset);
?>