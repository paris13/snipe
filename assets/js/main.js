
var activeNavID;
$( document ).ready(function() {
    //todo if fields added, show fieldlist, else show add field
});

$('.showEmptyField').click(function(){
    hideElements();
   
    try{
        map.removeLayer(markers);
       // removeExistingPolygons(); 
       // commented out on jan 11 2020 for allowing to create multiple field at once
    }
    catch{
        console.log("can't remove markers");
    };
    $("#addFieldEmpty").show();
    $("#addedFieldsNav").removeClass("active");
    activeNavID = "#newFieldNav";
});

$('#createField').click(function(){
    hideElements();
    $("#addFieldForm").show();
    //removeExistingPolygons();
    startEditing();
    map.setMaxZoom(23);
});



$('.field').click(function(){
    alert("Field clicked");
    hideElements();
    $("#fieldDetails").show();
});

function hideElements(){
    $("#line").show();
    $("#addFieldEmpty").hide();
    $("#addFieldForm").hide();
    // $("#fieldList").hide();
    $("#fieldDetails").hide();
    $("#dialog-tutorial").hide();
    $("#dialog-account").hide();
    $("#boxTimeline").hide();
    $("#map").css('height','100%');
    $("#ndviTitle").html(" Crop growth graph (NDVI)");

};

function showAddedFields(){
    hideElements();
    
    $("#fieldList").show();
    $("#newFieldNav").removeClass("active");
    $("#addedFieldsNav").addClass("active");
    map.setMaxZoom(17);
};

$("#tutorialNav").click(function(){
    $("#dialog-tutorial").dialog({
        resizable: false,
        height: "415",
        width: "660",
        modal: true
    });
});

$("#userAccount").click(function(){
    console.log("Opening dialog");
    $("#dialog-account").dialog({
        resizable: false,
        height: "50vh",
        width: "50vw",
        padding: "0",
        modal: true
    });
});

$("#dialog-account").closest(".ui-dialog").children(".ui-dialog-titlebar").css("background", "#06d633");