var plotDuration = 365;

function insightAndDuration (duration,insightType) {
    date = new Date();
    data = refineData(timelineData[insightType],date.setDate(date.getDate()-duration));
    if(insightType=='NDVI' || insightType=='NDWI'){
    updateChart(data,insightType);
    }
}


function chooseGraphDuration(button){
    plotDuration=button.value;
    insightAndDuration(plotDuration,insightType);

}

function insightChange(button){
    if(button.value =="Null"){
      removeExistingImages();  
    }
    
    else {
        try{
            insightType =  button.value;
            insightAndDuration(plotDuration,insightType);
            addLegend(insightType);
            if(insightImage[insightType]){
                drawImageonMap(insightImage[insightType],geometry);
            }    
        }
        catch(error){
            console.log(error);
        }
    }
}

function chartCreator(){
    date = new Date();
    data = refineData(timelineData[insightType],date.setDate(date.getDate()-plotDuration));
    if (!timelineOn) {
        plotChart(data,insightType);
        timelineOn = true;
    } else {
        updateChart(data,insightType);
    }
     
}

function refineData(data,startDate){
    chartData =[];
    for (i in data){
        if (data[i].x.getTime()>=startDate){
                chartData.push(data[i]);
        }      
    }
    return chartData;
}


function plotChart(data,insightType){
    var cha =document.getElementById('line').getContext('2d'); 
    
    configuration = {
        type: 'line',
        data:{
        datasets: [{
        lineTension: 0.2,
        data: data,
        label: insightType,
        borderColor: "#50C878",
        fill: true
        }]},

        options:{
            // onClick:function(){console.log(this['get data']);},
            title: {
            display: true,
            text: 'Crop growth'+" "+insightType
            },
            scales:{
                xAxes: [{
                type: 'time',
                time: {
                    unit: 'month'
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
                }],
                yAxes: [{
                    ticks:{
                        suggestedMin : 0.4,
                        suggestedMax : 0.8
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'value'
                    }
                }]
            }
        
        }
    };
    linechart = new Chart(cha,configuration);
}


function updateChart(data,insightType){
    if(insightType=='NDVI'){
        borderColor= "#50C878";
    }
    else if(insightType=='NDWI'){
        borderColor="#00CCCC";
    }
    
    linechart.data.datasets[0].data=data;
    linechart.data.datasets[0].label=insightType;
    linechart.data.datasets[0].borderColor=borderColor;
    linechart.options.title.text= 'Crop growth'+" "+ insightType;
    linechart.update();
}
