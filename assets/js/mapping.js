// const appId = "5oKWVyLqCBhc0WWWtm3r"
// const appCode = "-btAeHN-OEX7Y9pwG9pkQQ"
const url = 'https://randomuser.me/api/?results=2'; 

var srcTiles = [L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
})];
var map = L.map('map', {
    center: [39.489504, -121.814859],
    zoom: 13,
    layers: srcTiles,
    zoomControl: true
});


// FeatureGroup is to store editable layers
var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);

var drawControlFull = new L.Control.Draw({
    edit: {
        featureGroup: drawnItems
    },
    draw: {
        polyline: false
    }
});


var drawControlEditOnly = new L.Control.Draw({
    edit: {
        featureGroup: drawnItems
    },
    draw: false
});

map.addControl(drawControlFull);

map.on("draw:created", function (e) {
    var layer = e.layer;
    layer.addTo(drawnItems);
    drawControlFull.remove(map);
    drawControlEditOnly.addTo(map)
});

map.on("draw:deleted", function(e) {
    check =  Object.keys(drawnItems._layers).length;
    console.log(check);
     if (check === 0){
         drawControlEditOnly.remove(map);
         drawControlFull.addTo(map);
     };
 });
 
 var poly = {
    "type": "FeatureCollection",
    "features": [
      {
        "type": "Feature",
        "properties": {
          "farmID": "Farm4"
        },
        "geometry": {
          "type": "Polygon",
          "coordinates": [
            [
              [
                -121.81981801986694,
                39.48594230860115
              ],
              [
                -121.8104839324951,
                39.48594230860115
              ],
              [
                -121.8104839324951,
                39.492996772975424
              ],
              [
                -121.81981801986694,
                39.492996772975424
              ],
              [
                -121.81981801986694,
                39.48594230860115
              ]
            ]
          ]
        }
      }
    ]
  }
 var geojsonLayer = L.geoJson(poly);
 geojsonLayer.addTo(map);
// var exportButton = document.getElementById("export");
// var textarea = document.getElementById("json");
// exportButton.addEventListener("click", function(){
//     var geo = drawnItems.toGeoJSON();
//     console.log(geo);
//     textarea.innerHTML = JSON.stringify(geo);
// var bounds = geojsonLayer.getBounds();
// textarea.innerHTML = JSON.stringify(bounds);

// var imageUrl = "./assets/img/ndvi1.png",
//     imageBounds = [[37.892246, -121.562347], [38.005914, -121.216278]];
// var imagelayer = L.imageOverlay(imageUrl, imageBounds)
// imagelayer.addTo(map);

// var overlays = {
//     "GeoJson": geojsonLayer,
//     "NDVI": imagelayer
// };
// L.control.layers('',overlays).addTo(map);
// });

// var apiUrl = 'https://sarora.pythonanywhere.com/ndvi';
 
// var fetchButton = document.getElementById("fetch");
// fetchButton.addEventListener("click", function(){
//     fetch(apiUrl, {
//         method: 'POST', // or 'PUT'
//         body: JSON.stringify(poly), // data can be `string` or {object}!
//         headers:{
//           'Content-Type': 'application/json'
//         }
//       })
//       .then((resp) => resp.json())
//       .then(function(data){
//           console.log(data);
//       textarea.innerHTML = 'Success:' + JSON.stringify(data)
//     })
//       .catch(error => console.error('Error:', error)); 
// });

// var propButton = document.getElementById("prop");
// var farm = '6_Field1'

// propButton.addEventListener("click", function(){
//     poly['features'][0]['properties']['farmID'] = farm;

//     console.log(poly);
//     textarea.innerHTML = JSON.stringify(poly);
// });

 


