-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2019 at 12:35 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meow`
--

-- --------------------------------------------------------

--
-- Table structure for table `crops`
--

CREATE TABLE `crops` (
  `crop` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crops`
--

INSERT INTO `crops` (`crop`) VALUES
('banana'),
('barley'),
('corn'),
('cotton'),
('grapes'),
('jute'),
('pulse'),
('rice'),
('sugarcane'),
('wheat');

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `user_id` varchar(20) NOT NULL,
  `field_id` varchar(25) NOT NULL,
  `geo` varchar(1024) DEFAULT NULL,
  `crop` varchar(30) NOT NULL,
  `sowing_date` date NOT NULL,
  `location` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`user_id`, `field_id`, `geo`, `crop`, `sowing_date`, `location`) VALUES
('1', '1_Field1', '{\r\n	\"type\": \"FeatureCollection\",\r\n	\"features\": [{\r\n		\"type\": \"Feature\",\r\n		\"properties\": {},\r\n		\"geometry\": {\r\n			\"type\": \"Polygon\",\r\n			\"coordinates\": [\r\n				[\r\n					[-121.442184, 37.98107],\r\n					[-121.397552, 37.982152],\r\n					[-121.392746, 37.952935],\r\n					[-121.427078, 37.938322],\r\n					[-121.461411, 37.958346],\r\n					[-121.442184, 37.98107]\r\n				]\r\n			]\r\n		}\r\n	}]\r\n}', '', '0000-00-00', ''),
('1', '1_Field2', '{\r\n	\"type\": \"FeatureCollection\",\r\n	\"features\": [{\r\n		\"type\": \"Feature\",\r\n		\"properties\": {},\r\n		\"geometry\": {\r\n			\"type\": \"Polygon\",\r\n			\"coordinates\": [\r\n				[\r\n					[-121.442184, 37.98107],\r\n					[-121.397552, 37.982152],\r\n					[-121.392746, 37.952935],\r\n					[-121.427078, 37.938322],\r\n					[-121.461411, 37.958346],\r\n					[-121.442184, 37.98107]\r\n				]\r\n			]\r\n		}\r\n	}]\r\n}', '', '0000-00-00', ''),
('6', '6_Field1', '{\r\n	\"type\": \"FeatureCollection\",\r\n	\"features\": [{\r\n		\"type\": \"Feature\",\r\n		\"properties\": {},\r\n		\"geometry\": {\r\n			\"type\": \"Polygon\",\r\n			\"coordinates\": [\r\n				[\r\n					[-121.442184, 37.98107],\r\n					[-121.397552, 37.982152],\r\n					[-121.392746, 37.952935],\r\n					[-121.427078, 37.938322],\r\n					[-121.461411, 37.958346],\r\n					[-121.442184, 37.98107]\r\n				]\r\n			]\r\n		}\r\n	}]\r\n}', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `field_image`
--

CREATE TABLE `field_image` (
  `field_id` varchar(25) DEFAULT NULL,
  `sat_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `imageurl` varchar(512) NOT NULL,
  `NDVI` float NOT NULL,
  `gndvi` float NOT NULL,
  `yndvi` float NOT NULL,
  `rndvi` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `field_image`
--

INSERT INTO `field_image` (`field_id`, `sat_time`, `imageurl`, `NDVI`, `gndvi`, `yndvi`, `rndvi`) VALUES
('1_Field1', '2019-04-18 18:30:00', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.75, 9.3, 40.7, 50),
('6_Field1', '2019-04-19 18:30:00', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.75, 9.3, 40.7, 50),
('6_Field1', '2019-04-28 18:30:00', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.75, 9.3, 40.7, 50),
('1_Field1', '2019-05-17 15:20:23', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.45, 98.3, 40.7, 50),
('1_Field2', '2019-05-17 15:21:03', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.65, 59.9, 34.7, 30),
('1_Field2', '2019-05-17 15:22:16', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.75, 49.3, 44.7, 30),
('1_Field2', '2019-05-17 15:22:36', 'https://www.pythonanywhere.com/user/sarora/files/home/sarora/mysite/static/9892981044_2019-02-23_12%3A50%3A08_NDVI.png', 0.85, 7, 98, 45);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`) VALUES
('monu', 'monu'),
('manish', 'meowlmax'),
('jones', 'jonesjones'),
('manjones', 'newpass'),
('useruser', 'newuser'),
('vendetta', 'new'),
(NULL, NULL),
('manishjones', 'jimmyhowjones'),
('manishjones', 'meowlmax'),
('ajinkyapal', 'password'),
('akshar', '1234567'),
('monu', 'manish'),
('testuser', 'password'),
('piyushkumar9501', 'piyushku206020'),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
('monuunited', 'meowmeow'),
('jonesjones', 'manishjones'),
('jonesjj', 'jonesjones'),
('discodisco', 'discodisco'),
('cocococo', 'cocacola'),
('cocococoaa', 'cocacola'),
('mesutozilm10', 'mesutozilm10'),
('useruser', 'passpass'),
('userusert', 'passpass'),
('eliotjames', 'jamesisgood'),
('jameselliotgmailcom', 'manishjones'),
('jameselliotgmailcom', 'manishjones');

-- --------------------------------------------------------

--
-- Table structure for table `user_credentials`
--

CREATE TABLE `user_credentials` (
  `user_name` varchar(20) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_credentials`
--

INSERT INTO `user_credentials` (`user_name`, `user_id`, `password`) VALUES
('ajinkyapal', '2', 'password'),
('akshar', '3', '1234567'),
('cocococo', '8', 'cocacola'),
('cocococoaa', '9', 'cocacola'),
('eliotjames', '13', 'jamesisgood'),
('jameselliotgmailcom', '14', 'manishjones'),
('manishjones', '1', 'meowlmax'),
('mesutozilm10', '10', 'mesutozilm10'),
('monu', '4', 'manish'),
('monuunited', '7', 'meowmeow'),
('piyushkumar9501', '6', 'piyushku206020'),
('testuser', '5', 'password'),
('useruser', '11', 'passpass'),
('userusert', '12', 'passpass');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` varchar(20) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `m_name` varchar(20) DEFAULT NULL,
  `l_name` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `f_name`, `m_name`, `l_name`, `email`, `phone`) VALUES
('1', 'manish', '', 'jones', 'manishdavidjones@gmail.com', '8504826808'),
('10', 'mesut', '', 'ozil', 'mesutozil@m10.com', '9012345678'),
('11', 'meowmeow', 'meowmeow', 'meow', 'meow@meowmeow.in', '9876545678'),
('12', 'meowmeow', 'meowmeow', 'meow', 'meow@cannto.coco', '9876545677'),
('13', 'james', '', 'eliott', 'james@gmail.com', '6543217890'),
('14', 'manish', 'ma', 'ma', 'jameselliot@gmail.com', '9123456789'),
('2', 'ajinkya', '', 'pal', 'ajinkyapal@gmail.com', 'phonenumbe'),
('3', 'akshar ', '', '', 'aksharpadman@gmail.com', '8089229829'),
('4', 'monu', '', '', 'manish@gmail.com', '9835803801'),
('5', 'user', '', '', 'user@email.com', '1800180000'),
('6', 'piyush', '', 'Kumar', 'piyushkumar9501@gmail.com', '+91 723191'),
('7', 'manu', 'manu', 'manu', 'meowmeow@meow.com', '9876543210'),
('8', 'manish', 'lola', 'kumar', 'maneshdavidjones@gmail.com', '8504826908'),
('9', 'manish', 'hehe', 'kuku', 'maneedavidjones@gmail.com', '8504829908');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crops`
--
ALTER TABLE `crops`
  ADD PRIMARY KEY (`crop`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`field_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `field_image`
--
ALTER TABLE `field_image`
  ADD UNIQUE KEY `sat_time` (`sat_time`),
  ADD UNIQUE KEY `field_id` (`field_id`,`sat_time`);

--
-- Indexes for table `user_credentials`
--
ALTER TABLE `user_credentials`
  ADD PRIMARY KEY (`user_name`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`);

--
-- Constraints for table `field_image`
--
ALTER TABLE `field_image`
  ADD CONSTRAINT `field_image_ibfk_1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`field_id`);

--
-- Constraints for table `user_credentials`
--
ALTER TABLE `user_credentials`
  ADD CONSTRAINT `user_credentials_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
