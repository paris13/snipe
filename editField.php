<?php 

if(!isset($_SERVER['HTTP_REFERER']))
{     echo 'Unauthorized Access'; 
    exit();
}

/* this php file helps in creating a new field for the registerd user */
    session_start();
    include "./sqlconfig.php";
    
    
    $crop = $_POST['crop'];
    $fieldId = $_POST['fieldId'];
    $sowingDate = $_POST['sowingdate'];
    $location = $_POST['location'];
    $username = $_SESSION['name'];


    function insertUserFields($crop,$location,$sowingDate,$fieldId,$con){
        $insert="update fields set crop = ?, location = ?, sowing_date =? where field_id = ?";
        $stmt=$con->prepare($insert);
        $stmt->bind_param("ssss",$crop,$location,$sowingDate,$fieldId);
        $stmt->execute();
        $stmt->free_result();
    }

    insertUserFields($crop,$location,$sowingDate,$fieldId,$con) ;
    echo "$fieldId". "changes succesfully saved";

?>