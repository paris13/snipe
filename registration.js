var usernameforgotval;

$("#dialog-forgot").hide();
$("#otpDiv").hide();
$("#forgot").click(function(){
    $("#dialog-forgot").dialog({
        resizable: false,
        height: "50vh",
        width: "50vw",
        padding: "0",
        modal: true
    });
    $("#dialog-forgot :input").val("");
    $("#forgotDiv").show();
    $("#otpDiv").hide();
    $("#resetDiv").hide();
});

$("#sendOTP").click(function(){
    checkNameorMail();
});

$("#confirmOTP").click(function(){
    confirmOTP();
});

$("#resetPassword").click(function(){
    resetPassword();
});

function checkNameorMail(){
    if($("#usernameforgot").val()){
        usernameforgotval = $("#usernameforgot").val();
        //php runs and returns
        if(1){
            //CHECK IF EMAIL EXISTS
        // sendOTP();
        $("#forgotDiv").hide();
        $("#otpDiv").show();
        $("#resetDiv").hide();
    }
    else {
        alert("No account exists for this Username/E-mail. Please sign up.");
        $("#usernameforgot").val("");
    }
    }
    else {
        alert("Enter your username or E-mail");
    };
};

function sendOTP(){
    console.log("sent OTP");
};

function confirmOTP(){
    if($("#otp").val()){
        if(true){
    console.log("confirmed");
    $("#forgotDiv").hide();
        $("#otpDiv").hide();
        $("#resetDiv").show();
        }
        else {
            alert("Invalid OTP");
            $("#otp").val("");
        };
    }
    else {
        alert("Enter an OTP");
    }
};

function resetPassword(){
    if($("#newpass").val().length>=8 && $("#newpass").val() == $("#newpassconfirm").val()){
        alert("Password reset. Now you can login with the new password");
        $("#dialog-forgot").dialog('close');
    }
    else{
        alert("Please enter matching passwords of minimum 8 characters.")
    }
};

username=document.getElementById("user_name");
fname=document.getElementById("fname");
email=document.getElementById("email");
phone=document.getElementById("phone");
mname=document.getElementById("mname");
lname=document.getElementById("lname");
pass=document.getElementById("pass");
cpass=document.getElementById("cpass");
signuplink = document.getElementById("signuplink");
loginlink = document.getElementById("loginlink");
loginForm = document.getElementById("loginForm");
registerForm = document.getElementById("registerForm");
signuplink.addEventListener("click", function(){
    loginForm.style.display = "none";
    registerForm.style.display = ""

});
var globalarray=[];
loginlink.addEventListener("click", function(){
    loginForm.style.display = "";
    registerForm.style.display = "none"

});

function validateName (str){
    
    if(str.length>=0 && str.length<=20){
        return true;
    }
    else false;
}

function validateUserName(str){
    
    var x=/^([a-z,A-Z,0-9,_\-]){1,20}$/;
    //var x=/^[a-z,A-Z,0-9]+$/;
    
   var y=x.test(str);
        //console.log(x);
    if(y){
        if(str.length>=6 && str.length<=20){
            username.style="box-shadow: 0px 0px 8px 0px #00ff00 "; 
            return y;
        }
        else return false;
    }
    else {
        username.style="box-shadow: 0px 0px 8px 0px #ff0000 ";
        return false;
    }
}

function validatePhone(str){
    var x=/^([0-9]{1})([0-9]{9})$/;
    var y=x.test(str);
    if(!y){
        phone.style="box-shadow: 0px 0px 8px 0px #ff0000 ";
    }
    else{
        phone.style="box-shadow:0px 0px 8px 0px #00ff00";
    }
    return y;
}

function validateEmail(str){
    var x=/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var y=x.test(str);
    if(!y){
        email.style="box-shadow: 0px 0px 8px 0px #ff0000 ";
    }
    else {
        email.style="box-shadow:0px 0px 8px 0px #00ff00";
    }
    return y;
}

function validatePassword(){
    pass=document.getElementById("pass");
    cpass=document.getElementById("cpass");
    var aa=/^([a-z,A-Z,0-9,_\-\.\$!@#%\^&\*]){1,20}$/;
    var y=aa.test(pass.value);
    if (y){
        if(pass.value.length>=8 && pass.value.length<=20){
            
            if (pass.value===cpass.value){
                // cpass.style="box-shadow: 0px";
                cpass.style="box-shadow: 0px 0px 8px 0px #00ff00 ";
                pass.style="box-shadow: 0px 0px 8px 0px #00ff00 ";
                // pass.style="box-shadow: 0px";
                return y;
            }
            else {
                cpass.style="box-shadow: 0px 0px 8px 0px #ff0000 ";
                return false;
            }
        } 
        else {
            pass.style="box-shadow: 0px 0px 6px 0px #ff0000 ";
            pass.style="background:#ff0000";
            return false;
        }  
    }
    else {
        pass.style="box-shadow: 0px 0px 6px 0px #ff0000 ";
        pass.style="background:#ff0000";
        return false;
    }
}

function validation(){
    
    var u=validateUserName(username.value);
    //console.log(username.value);
    var f=validateName(fname.value);
    var l=validateName(lname.value);
    var m=validateName(mname.value);
    var e=validateEmail(email.value);
    // var p=validatePhone(phone.value);
    var p=true;
    var pass=validatePassword();
    console.log(f,l,m,e,p,pass,globalarray[8],globalarray[12],globalarray[14]);
    if (f && l && m && e && pass && u && globalarray[8] && globalarray[12] && globalarray[14] ){
        return true;
    }

    else return false;
}
function userCheck(ele){
    var valid;
    if(ele.value===username.value){
        valid=validateUserName(username.value);
    }
    else if(ele.value===email.value){
        valid=validateEmail(email.value);
    }
    else {
        // valid=validatePhone(phone.value);
        valid=true;
    }

    if(valid){   
        //console.log(ele.className);
        var data="val="+ele.value+"&&eleID="+ele.id+"&&eleClass="+ele.name;
        var xhr=new XMLHttpRequest();
        
        xhr.onreadystatechange=function(){
            
            if(this.readyState==4 && this.status==200){;
                
                if(this.responseText==1){
                    console.log(this.responseText);
                    ele.style="box-shadow: 0px 0px 5px 2px #ff000066 !important; background-color: #ff000066 !important";
                    // console.log(ele.placeholder.length);
                    // console.log(ele.style);
                    globalarray[ele.placeholder.length]=false;
                    alert("This "+ele.placeholder+" is already taken please choose another one");
                    
                }
                else{
                    globalarray[ele.placeholder.length]=true;
                    ele.style="box-shadow: 0px 0px 5px 2px #495057 !important; background-color:#ffffff";
                }   
            }
        }
        xhr.open("GET",'./registrationuser.php?'+data,true);
        xhr.send();
    }
}

function dataSubmit(){
    if (validation()){
        console.log("js val done");
        var data="username="+username.value+"&fname="+fname.value+"&mname="+mname.value+"&lname="+lname.value+
        "&email="+email.value+"&phone="+phone.value+"&pass="+pass.value+"&cpass="+cpass.value;
        console.log(data);
        var xhr=new XMLHttpRequest();
        xhr.open("POST","./registrationuser.php",true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //xhr.setRequestHeader("Content-length", data.length);
        //xhr.setRequestHeader("Connection", "close");
        xhr.onreadystatechange=function(){
            if(this.readyState==4 && this.status==200){
                console.log(this.responseText);
                // document.getElementById("messageform").innerHTML=this.responseText;
                if(this.responseText.includes("success")){
                    alert("Registration successful! \n now you can login via this credentials");
                    window.location.href="https://snipe.agriforetell.com";
                }
            }
        }
        xhr.send(data);
    }

    else { alert("validation failed");}
     
}

